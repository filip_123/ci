/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-17-2017 14.23.46
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Ddv_obveznosti


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Ddv_obveznosti]
Print 'Create Table [dbo].[Ddv_obveznosti]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Ddv_obveznosti] (
		[identity_fakture]       [int] NOT NULL,
		[id]                     [int] IDENTITY(1, 1) NOT NULL,
		[pozicija]               [tinyint] NOT NULL,
		[vrsta_knjizbe]          [tinyint] NOT NULL,
		[stopnja]                [tinyint] NOT NULL,
		[vrsta_prometa]          [tinyint] NOT NULL,
		[stroskovno_mesto]       [varchar](6) NULL,
		[stroskovni_nosilec]     [varchar](6) NULL,
		[opis_knjizbe]           [varchar](60) NULL,
		[osnova_ddv]             [money] NULL,
		[znesek]                 [money] NULL,
		[oprostitve]             [tinyint] NULL,
		[projekt]                [int] NULL,
		CONSTRAINT [PK_Ddv_obveznosti]
		PRIMARY KEY
		CLUSTERED
		([identity_fakture], [id])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	ADD
	CONSTRAINT [DF_Ddv_obveznosti_pozicija]
	DEFAULT ((0)) FOR [pozicija]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Ddv_stopnja]
	FOREIGN KEY ([stopnja]) REFERENCES [dbo].[Ddv_stopnja] ([sifra])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Ddv_stopnja]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Ddv_vrsta_prometa]
	FOREIGN KEY ([vrsta_prometa]) REFERENCES [dbo].[Ddv_vrsta_prometa] ([sifra])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Ddv_vrsta_prometa]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Izhodna_faktura]
	FOREIGN KEY ([identity_fakture]) REFERENCES [dbo].[Izhodna_faktura] ([identity_fakture])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Izhodna_faktura]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Projekt]
	FOREIGN KEY ([projekt]) REFERENCES [dbo].[Projekt] ([sifra])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Projekt]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Stroskovni_nosilec]
	FOREIGN KEY ([stroskovni_nosilec]) REFERENCES [dbo].[Stroskovni_nosilec] ([sifra])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Stroskovni_nosilec]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Stroskovno_mesto]
	FOREIGN KEY ([stroskovno_mesto]) REFERENCES [dbo].[Stroskovno_mesto] ([sifra])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Stroskovno_mesto]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Ddv_obveznosti]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Ddv_obveznosti_Vrsta_knjizbe]
	FOREIGN KEY ([vrsta_knjizbe]) REFERENCES [dbo].[Vrsta_knjizbe] ([vrsta])
ALTER TABLE [dbo].[Ddv_obveznosti]
	CHECK CONSTRAINT [FK_Ddv_obveznosti_Vrsta_knjizbe]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
