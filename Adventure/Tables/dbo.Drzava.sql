/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-17-2017 14.23.46
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Drzava


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Drzava]
Print 'Create Table [dbo].[Drzava]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Drzava] (
		[sifra]                 [varchar](6) NOT NULL,
		[naziv]                 [varchar](40) NULL,
		[mednarodna_oznaka]     [varchar](4) NULL,
		[angleski_naziv]        [varchar](40) NULL,
		[koda_drzave]           [varchar](6) NULL,
		[valuta]                [varchar](6) NULL,
		[nemski_naziv]          [varchar](40) NULL,
		[eu]                    [bit] NOT NULL,
		[casovna_znacka]        [datetime] NULL,
		[interni_komentar]      [varchar](60) NULL,
		[drzava_id]             [int] NULL,
		[binary_hash]           [int] NULL,
		CONSTRAINT [PK_Drzava]
		PRIMARY KEY
		NONCLUSTERED
		([sifra])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Drzava]
	ADD
	CONSTRAINT [DF_Drzava_casovna_znacka]
	DEFAULT (getdate()) FOR [casovna_znacka]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Drzava]
	ADD
	CONSTRAINT [DF_Drzava_eu]
	DEFAULT ((0)) FOR [eu]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Drzava] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Drzava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Drzava_Valuta]
	FOREIGN KEY ([valuta]) REFERENCES [dbo].[Valuta] ([sifra])
ALTER TABLE [dbo].[Drzava]
	CHECK CONSTRAINT [FK_Drzava_Valuta]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
