/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-17-2017 14.23.46
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Poslovni_partner


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Poslovni_partner]
Print 'Create Table [dbo].[Poslovni_partner]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Poslovni_partner] (
		[sifra]                             [varchar](7) NOT NULL,
		[stara_sifra]                       [varchar](15) NULL,
		[sifra_stara]                       [varchar](15) NULL,
		[naziv_1]                           [varchar](35) NULL,
		[naziv_2]                           [varchar](35) NULL,
		[naziv_3]                           [varchar](170) NULL,
		[posta]                             [varchar](10) NOT NULL,
		[drzava]                            [varchar](6) NOT NULL,
		[ulica]                             [varchar](60) NULL,
		[telefon_1]                         [varchar](20) NULL,
		[telefon_2]                         [varchar](20) NULL,
		[fax]                               [varchar](20) NULL,
		[davcna_stevilka]                   [varchar](8) NULL,
		[maticna_stevilka]                  [varchar](7) NULL,
		[sifra_agenta]                      [varchar](6) NULL,
		[konto_kupca]                       [dbo].[konti] NULL,
		[konto_dobavitelja]                 [dbo].[konti] NULL,
		[konto_dev_kupca]                   [dbo].[konti] NULL,
		[konto_dev_dobavitelja]             [dbo].[konti] NULL,
		[je_kupec]                          [bit] NOT NULL,
		[je_dobavitelj]                     [bit] NOT NULL,
		[sedez_slovenija]                   [bit] NOT NULL,
		[je_zaveznec_ddv]                   [bit] NOT NULL,
		[se_racunajo_obresti]               [bit] NOT NULL,
		[sifra_obresti]                     [int] NOT NULL,
		[neto_dni_obresti]                  [int] NOT NULL,
		[do_dni_obresti]                    [int] NULL,
		[sifra_do_dni_obresti]              [int] NULL,
		[vrsta_partnerja_si]                [varchar](6) NULL,
		[casovna_znacka]                    [datetime] NULL,
		[interni_komentar]                  [varchar](60) NULL,
		[do_dni_ponderirane_obresti]        [int] NULL,
		[povečano_odstotnih_tock]           [float] NULL,
		[ponderirane_po_datumu_valute]      [bit] NOT NULL,
		[ponderirane_po_datumu_placila]     [bit] NOT NULL,
		[poslovni_partner_id]               [int] NULL,
		[binary_hash]                       [int] NULL,
		CONSTRAINT [PK_Poslovni_partner]
		PRIMARY KEY
		CLUSTERED
		([sifra])
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_casovna_znacka]
	DEFAULT (getdate()) FOR [casovna_znacka]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_je_dobavitelj]
	DEFAULT ((1)) FOR [je_dobavitelj]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_je_kupec]
	DEFAULT ((1)) FOR [je_kupec]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_je_zaveznec_ddv]
	DEFAULT ((1)) FOR [je_zaveznec_ddv]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_neto_dni_obresti]
	DEFAULT ((0)) FOR [neto_dni_obresti]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_ponderirane_po_datumu_placila]
	DEFAULT ((0)) FOR [ponderirane_po_datumu_placila]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_ponderirane_po_datumu_valute]
	DEFAULT ((0)) FOR [ponderirane_po_datumu_valute]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_se_racunajo_obresti]
	DEFAULT ((1)) FOR [se_racunajo_obresti]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_sedez_slovenija]
	DEFAULT ((1)) FOR [sedez_slovenija]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	ADD
	CONSTRAINT [DF_Poslovni_partner_sifra_obresti]
	DEFAULT ((1)) FOR [sifra_obresti]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_Poslovni_partner_drzava]
	ON [dbo].[Poslovni_partner] ([drzava])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_bindefault @defname=N'[dbo].[fingerprint]', @objname=N'[dbo].[Poslovni_partner].[interni_komentar]'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Agent]
	FOREIGN KEY ([sifra_agenta]) REFERENCES [dbo].[Agent] ([sifra])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Agent]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Drzava]
	FOREIGN KEY ([drzava]) REFERENCES [dbo].[Drzava] ([sifra])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Drzava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Kontni_plan]
	FOREIGN KEY ([konto_kupca]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Kontni_plan]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Kontni_plan1]
	FOREIGN KEY ([konto_dobavitelja]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Kontni_plan1]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Kontni_plan2]
	FOREIGN KEY ([konto_dev_kupca]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Kontni_plan2]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Kontni_plan3]
	FOREIGN KEY ([konto_dev_dobavitelja]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Kontni_plan3]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_Posta]
	FOREIGN KEY ([posta], [drzava]) REFERENCES [dbo].[Posta] ([postna_stevilka], [drzava])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_Posta]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Poslovni_partner]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Poslovni_partner_sif_Vrste_partnerjev]
	FOREIGN KEY ([vrsta_partnerja_si]) REFERENCES [dbo].[sif_Vrste_partnerjev] ([vrsta_partnerja_si])
ALTER TABLE [dbo].[Poslovni_partner]
	CHECK CONSTRAINT [FK_Poslovni_partner_sif_Vrste_partnerjev]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
