/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-17-2017 14.23.46
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Posta


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Posta]
Print 'Create Table [dbo].[Posta]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Posta] (
		[postna_stevilka]      [varchar](10) NOT NULL,
		[drzava]               [varchar](6) NOT NULL,
		[mesto]                [varchar](40) NULL,
		[casovna_znacka]       [datetime] NULL,
		[interni_komentar]     [varchar](60) NULL,
		[posta_id]             [int] NULL,
		[binary_hash]          [int] NULL,
		CONSTRAINT [PK_Posta]
		PRIMARY KEY
		NONCLUSTERED
		([postna_stevilka], [drzava])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Posta]
	ADD
	CONSTRAINT [DF_Posta_casovna_znacka]
	DEFAULT (getdate()) FOR [casovna_znacka]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Posta] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Posta]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Posta_Drzava]
	FOREIGN KEY ([drzava]) REFERENCES [dbo].[Drzava] ([sifra])
ALTER TABLE [dbo].[Posta]
	CHECK CONSTRAINT [FK_Posta_Drzava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
