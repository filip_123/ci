/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-17-2017 14.23.46
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Vrsta_knjizbe


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Vrsta_knjizbe]
Print 'Create Table [dbo].[Vrsta_knjizbe]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Vrsta_knjizbe] (
		[vrsta]                        [tinyint] NOT NULL,
		[opis]                         [varchar](30) NULL,
		[zapira]                       [char](10) NULL,
		[zapira_privzeto]              [char](10) NULL,
		[obveznost_do_dobavitelja]     [bit] NOT NULL,
		[obveznost_do_kupca]           [bit] NOT NULL,
		[obracun_prom_davka]           [bit] NOT NULL,
		[obresti]                      [bit] NOT NULL,
		[int_vrsta]                    [tinyint] NULL,
		[konto_predplacil]             [dbo].[konti] NULL,
		[loceno_stevilcenje]           [bit] NOT NULL,
		[obdobje_knjizenja]            [tinyint] NOT NULL,
		CONSTRAINT [PK_Vrsta_knjizbe]
		PRIMARY KEY
		NONCLUSTERED
		([vrsta])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	ADD
	CONSTRAINT [DF_Vrsta_knjizbe_obdobje_knjizenja]
	DEFAULT ((1)) FOR [obdobje_knjizenja]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	ADD
	CONSTRAINT [DF_Vrsta_knjizbe_obracun_prom_davka]
	DEFAULT ((0)) FOR [obracun_prom_davka]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	ADD
	CONSTRAINT [DF_Vrsta_knjizbe_obresti]
	DEFAULT ((0)) FOR [obresti]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	ADD
	CONSTRAINT [DF_Vrsta_knjizbe_obveznost_do_dobavitelja]
	DEFAULT ((0)) FOR [obveznost_do_dobavitelja]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	ADD
	CONSTRAINT [DF_Vrsta_knjizbe_obveznost_do_kupca]
	DEFAULT ((0)) FOR [obveznost_do_kupca]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	ADD
	CONSTRAINT [DF_Vrsta_knjizbe_samostojno_stevilcenje]
	DEFAULT ((0)) FOR [loceno_stevilcenje]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [Vrsta_knjizbe1]
	ON [dbo].[Vrsta_knjizbe] ([vrsta], [opis])
	WITH ( FILLFACTOR = 90)
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_Vrsta_knjizbe_vrsta_int_vrsta]
	ON [dbo].[Vrsta_knjizbe] ([vrsta], [int_vrsta])
	WITH ( FILLFACTOR = 90)
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Vrsta_knjizbe]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Vrsta_knjizbe_Kontni_plan]
	FOREIGN KEY ([konto_predplacil]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Vrsta_knjizbe]
	CHECK CONSTRAINT [FK_Vrsta_knjizbe_Kontni_plan]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
