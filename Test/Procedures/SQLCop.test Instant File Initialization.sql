/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Procedures:  test Instant File Initialization


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Procedure [SQLCop].[test Instant File Initialization]
Print 'Create Procedure [SQLCop].[test Instant File Initialization]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


CREATE PROCEDURE [SQLCop].[test Instant File Initialization]
AS
BEGIN
	-- Written by George Mastros
	-- February 25, 2012
	-- http://sqlcop.lessthandot.com
	-- http://www.bradmcgehee.com/2010/07/instant-file-initialization-speeds-sql-server/#more-1704
	
    SET NOCOUNT ON

	DECLARE @Output VarChar(max)
	SET @Output = ''

    CREATE TABLE #Output(Value VarChar(8000))

	If Exists(select * from sys.configurations Where name='xp_cmdshell' and value_in_use = 1)
		Begin
			If Is_SrvRoleMember('sysadmin') = 1
				Begin
					Insert Into #Output EXEC ('xp_cmdshell ''whoami /priv''');

					If Not Exists(Select 1 From #Output Where Value LIKE '%SeManageVolumePrivilege%')
						Select @Output = 'Instant File Initialization disabled'
					Else
						Select	@Output = 'Instant File Initialization disabled'
						From	#Output
						Where	Value LIKE '%SeManageVolumePrivilege%'
								And Value Like '%disabled%'
				End
			Else
				Set @Output = 'You do not have the appropriate permissions to run xp_cmdshell'
		End
	Else
		Begin
			Set @Output = 'xp_cmdshell must be enabled to determine if instant file initialization is enabled.'
		End
	Drop Table #Output

	If @Output > '' 
		Begin
			Set @Output = Char(13) + Char(10) 
						  + 'For more information:  '
						  + 'http://www.bradmcgehee.com/2010/07/instant-file-initialization-speeds-sql-server/#more-1704'
						  + Char(13) + Char(10) 
						  + Char(13) + Char(10) 
						  + @Output
			EXEC tSQLt.Fail @Output
		End  
END;
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
