/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Analiza_V3_definicija


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Analiza_V3_definicija]
Print 'Create Table [dbo].[Analiza_V3_definicija]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Analiza_V3_definicija] (
		[index_glave]         [int] NOT NULL,
		[sifra_aop]           [char](4) NOT NULL,
		[sort]                [char](5) NULL,
		[konto]               [dbo].[konti] NULL,
		[bold_v_izpisu]       [bit] NOT NULL,
		[naziv_vrstice]       [varchar](200) NULL,
		[formula_01]          [varchar](1024) NULL,
		[formula_02]          [varchar](1024) NULL,
		[formula_03]          [varchar](1024) NULL,
		[formula_04]          [varchar](1024) NULL,
		[formula_05]          [varchar](1024) NULL,
		[formula_06]          [varchar](1024) NULL,
		[formula_07]          [varchar](1024) NULL,
		[formula_08]          [varchar](1024) NULL,
		[formula_09]          [varchar](1024) NULL,
		[formula_10]          [varchar](1024) NULL,
		[formula_11]          [varchar](1024) NULL,
		[formula_12]          [varchar](1024) NULL,
		[formula_13]          [varchar](1024) NULL,
		[formula_14]          [varchar](1024) NULL,
		[formula_15]          [varchar](1024) NULL,
		[nova_sifra_aop]      [char](4) NULL,
		[stara_sifra_aop]     [varchar](4) NULL,
		[izpis_vrstice]       [int] NULL,
		CONSTRAINT [PK_Analiza_V3_definicija]
		PRIMARY KEY
		CLUSTERED
		([index_glave], [sifra_aop])
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_definicija]
	ADD
	CONSTRAINT [DF_Analiza_V3_definicija_bold_v_izpisu]
	DEFAULT ((0)) FOR [bold_v_izpisu]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_definicija]
	ADD
	CONSTRAINT [DF_Analiza_V3_definicija_naziv_vrstice]
	DEFAULT ((0)) FOR [naziv_vrstice]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_definicija] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
