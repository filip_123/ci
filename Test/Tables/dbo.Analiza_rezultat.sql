/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Analiza_rezultat


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Analiza_rezultat]
Print 'Create Table [dbo].[Analiza_rezultat]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Analiza_rezultat] (
		[identity_analize]     [int] IDENTITY(1, 1) NOT NULL,
		[index_glave]          [int] NOT NULL,
		[sifra_aop]            [char](4) NOT NULL,
		[sm]                   [varchar](6) NULL,
		[projekt]              [int] NULL,
		[leto]                 [smallint] NULL,
		[mesec]                [smallint] NULL,
		[znesek]               [money] NULL,
		CONSTRAINT [PK_Analiza_rezultat]
		PRIMARY KEY
		NONCLUSTERED
		([identity_analize])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE CLUSTERED INDEX [IX_Analiza_rezultat]
	ON [dbo].[Analiza_rezultat] ([index_glave], [sm], [projekt], [leto], [mesec], [sifra_aop])
	WITH ( FILLFACTOR = 90)
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_rezultat] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_rezultat]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_rezultat_Analiza_pozicija]
	FOREIGN KEY ([index_glave], [sifra_aop]) REFERENCES [dbo].[Analiza_pozicija] ([index_glave], [sifra_aop])
ALTER TABLE [dbo].[Analiza_rezultat]
	CHECK CONSTRAINT [FK_Analiza_rezultat_Analiza_pozicija]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_rezultat]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_rezultat_Obdobje]
	FOREIGN KEY ([leto], [mesec]) REFERENCES [dbo].[Obdobje] ([leto], [mesec])
ALTER TABLE [dbo].[Analiza_rezultat]
	CHECK CONSTRAINT [FK_Analiza_rezultat_Obdobje]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_rezultat]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_rezultat_Projekt]
	FOREIGN KEY ([projekt]) REFERENCES [dbo].[Projekt] ([sifra])
ALTER TABLE [dbo].[Analiza_rezultat]
	CHECK CONSTRAINT [FK_Analiza_rezultat_Projekt]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_rezultat]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_rezultat_Stroskovno_mesto]
	FOREIGN KEY ([sm]) REFERENCES [dbo].[Stroskovno_mesto] ([sifra])
ALTER TABLE [dbo].[Analiza_rezultat]
	CHECK CONSTRAINT [FK_Analiza_rezultat_Stroskovno_mesto]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
