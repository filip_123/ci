/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Devizni_tecaj


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Devizni_tecaj]
Print 'Create Table [dbo].[Devizni_tecaj]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Devizni_tecaj] (
		[banka_tecaj]          [tinyint] NOT NULL,
		[datum_tecaja]         [smalldatetime] NOT NULL,
		[sifra_valute]         [varchar](6) NOT NULL,
		[tip_tecaja]           [tinyint] NOT NULL,
		[enota]                [int] NULL,
		[tecaj]                [money] NULL,
		[interni_komentar]     [varchar](60) NULL,
		CONSTRAINT [PK_Devizni_tecaj]
		PRIMARY KEY
		NONCLUSTERED
		([banka_tecaj], [datum_tecaja], [sifra_valute], [tip_tecaja])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Devizni_tecaj]
	ADD
	CONSTRAINT [DF_Devizni_tecaj_interni_komentar]
	DEFAULT (left((((host_name()+' ')+CONVERT([varchar],getdate(),(12)))+',')+left(CONVERT([varchar],getdate(),(8)),(5)),(60))) FOR [interni_komentar]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_Devizni_tecaj_datum_tecaja]
	ON [dbo].[Devizni_tecaj] ([datum_tecaja])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Devizni_tecaj] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Devizni_tecaj]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Devizni_tecaj_Banka_devizni_tecaj]
	FOREIGN KEY ([banka_tecaj]) REFERENCES [dbo].[Banka_devizni_tecaj] ([sifra_banke])
ALTER TABLE [dbo].[Devizni_tecaj]
	CHECK CONSTRAINT [FK_Devizni_tecaj_Banka_devizni_tecaj]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Devizni_tecaj]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Devizni_tecaj_Tip_tecaja]
	FOREIGN KEY ([tip_tecaja]) REFERENCES [dbo].[Tip_tecaja] ([tip])
ALTER TABLE [dbo].[Devizni_tecaj]
	CHECK CONSTRAINT [FK_Devizni_tecaj_Tip_tecaja]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Devizni_tecaj]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Devizni_tecaj_Valuta]
	FOREIGN KEY ([sifra_valute]) REFERENCES [dbo].[Valuta] ([sifra])
ALTER TABLE [dbo].[Devizni_tecaj]
	CHECK CONSTRAINT [FK_Devizni_tecaj_Valuta]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
