/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Kontni_plan


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Kontni_plan]
Print 'Create Table [dbo].[Kontni_plan]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Kontni_plan] (
		[konto]                    [dbo].[konti] NOT NULL,
		[opis]                     [varchar](255) NULL,
		[tip_knjigovodstva]        [tinyint] NOT NULL,
		[sintetika]                [bit] NOT NULL,
		[ima_sm]                   [bit] NOT NULL,
		[devizni_konto]            [bit] NOT NULL,
		[konto_kupca]              [bit] NOT NULL,
		[konto_dobavitelja]        [bit] NOT NULL,
		[obresti]                  [bit] NOT NULL,
		[knjizi_kumulativno]       [bit] NOT NULL,
		[obresti_devizne]          [bit] NOT NULL,
		[izvenbilancni]            [bit] NOT NULL,
		[ima_analitika4]           [bit] NOT NULL,
		[breme_dobro]              [char](1) NOT NULL,
		[negativna_kumulativa]     [bit] NOT NULL,
		[konto_id]                 [int] NULL,
		[binary_hash]              [int] NULL,
		CONSTRAINT [PK_Kontni_plan]
		PRIMARY KEY
		NONCLUSTERED
		([konto])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	WITH NOCHECK
	ADD
	CONSTRAINT [CK_Kontni_plan_breme_dobro]
	CHECK
	([breme_dobro]='B' OR [breme_dobro]='D' OR [breme_dobro]='_')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
CHECK CONSTRAINT [CK_Kontni_plan_breme_dobro]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	WITH NOCHECK
	ADD
	CONSTRAINT [CK_Kontni_plan_tip_knjigovodstva]
	CHECK
	([tip_knjigovodstva]=(0) OR [tip_knjigovodstva]=(1) OR [tip_knjigovodstva]=(2))
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
CHECK CONSTRAINT [CK_Kontni_plan_tip_knjigovodstva]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_breme_dobro_1]
	DEFAULT ('_') FOR [breme_dobro]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_devizni_konto]
	DEFAULT ((0)) FOR [devizni_konto]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_ima_analitika4]
	DEFAULT ((0)) FOR [ima_analitika4]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_ima_sm]
	DEFAULT ((0)) FOR [ima_sm]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_izvenbilancni]
	DEFAULT ((0)) FOR [izvenbilancni]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_knjizi_kumulativno]
	DEFAULT ((0)) FOR [knjizi_kumulativno]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_konto_dobavitelja]
	DEFAULT ((0)) FOR [konto_dobavitelja]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_konto_kupca]
	DEFAULT ((0)) FOR [konto_kupca]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_negativna_kumulativa_1]
	DEFAULT ((1)) FOR [negativna_kumulativa]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_obresti]
	DEFAULT ((0)) FOR [obresti]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_obresti_devizne]
	DEFAULT ((0)) FOR [obresti_devizne]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_sintetika]
	DEFAULT ((0)) FOR [sintetika]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan]
	ADD
	CONSTRAINT [DF_Kontni_plan_tip_knjigovodstva]
	DEFAULT ((1)) FOR [tip_knjigovodstva]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Kontni_plan] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
