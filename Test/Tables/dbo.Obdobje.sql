/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Obdobje


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Obdobje]
Print 'Create Table [dbo].[Obdobje]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Obdobje] (
		[leto]                         [smallint] NOT NULL,
		[mesec]                        [smallint] NOT NULL,
		[zaprto]                       [bit] NOT NULL,
		[zaprto_ddv]                   [bit] NOT NULL,
		[ddv_pavsal_procent]           [money] NOT NULL,
		[ddv_pavsal_procent_arhiv]     [money] NULL,
		[obdobje_op]                   AS ([leto]*(100)+[mesec]),
		CONSTRAINT [PK_Obdobje]
		PRIMARY KEY
		CLUSTERED
		([leto], [mesec])
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obdobje]
	ADD
	CONSTRAINT [DF_Obdobje_ddv_pavsal_procent]
	DEFAULT ((100)) FOR [ddv_pavsal_procent]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obdobje]
	ADD
	CONSTRAINT [DF_Obdobje_zaprto]
	DEFAULT ((1)) FOR [zaprto]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obdobje]
	ADD
	CONSTRAINT [DF_Obdobje_zaprto_ddv]
	DEFAULT ((1)) FOR [zaprto_ddv]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obdobje] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obdobje]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Obdobje_Aktivno_leto]
	FOREIGN KEY ([leto]) REFERENCES [dbo].[Aktivno_leto] ([leto])
ALTER TABLE [dbo].[Obdobje]
	CHECK CONSTRAINT [FK_Obdobje_Aktivno_leto]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
