/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Private_AssertEqualsTableSchema_Actual


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [tSQLt].[Private_AssertEqualsTableSchema_Actual]
Print 'Create Table [tSQLt].[Private_AssertEqualsTableSchema_Actual]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [tSQLt].[Private_AssertEqualsTableSchema_Actual] (
		[name]                [nvarchar](256) NULL,
		[RANK(column_id)]     [int] NULL,
		[system_type_id]      [nvarchar](max) NULL,
		[user_type_id]        [nvarchar](max) NULL,
		[max_length]          [smallint] NULL,
		[precision]           [tinyint] NULL,
		[scale]               [tinyint] NULL,
		[collation_name]      [nvarchar](256) NULL,
		[is_nullable]         [bit] NULL,
		[is_identity]         [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [tSQLt].[Private_AssertEqualsTableSchema_Actual] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
