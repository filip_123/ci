/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  TestResult


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [tSQLt].[TestResult]
Print 'Create Table [tSQLt].[TestResult]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [tSQLt].[TestResult] (
		[Id]                [int] IDENTITY(1, 1) NOT NULL,
		[Class]             [nvarchar](max) NOT NULL,
		[TestCase]          [nvarchar](max) NOT NULL,
		[Name]              AS ((quotename([Class])+'.')+quotename([TestCase])),
		[TranName]          [nvarchar](max) NOT NULL,
		[Result]            [nvarchar](max) NULL,
		[Msg]               [nvarchar](max) NULL,
		[TestStartTime]     [datetime] NOT NULL,
		[TestEndTime]       [datetime] NULL,
		CONSTRAINT [PK__TestResu__3214EC0799DBDEA6]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [tSQLt].[TestResult]
	ADD
	CONSTRAINT [DF:TestResult(TestStartTime)]
	DEFAULT (getdate()) FOR [TestStartTime]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [tSQLt].[TestResult] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
