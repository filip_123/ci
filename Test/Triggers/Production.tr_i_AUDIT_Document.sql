/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_i_AUDIT_Document


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [Production].[tr_i_AUDIT_Document]
Print 'Create Trigger [Production].[tr_i_AUDIT_Document]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [Production].[tr_i_AUDIT_Document]
ON [Production].[Document]
FOR INSERT


	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  INSERT TRIGGER for Table:  [Production].[Document]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.13
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE				varchar(50),
		@AUDIT_LOG_TRANSACTION_ID		Int,
		@PRIM_KEY				nvarchar(4000),
		--@TABLE_NAME				nvarchar(4000),
		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[Production].[Document]'
	Select @ROWS_COUNT=count(*) from inserted
	Set @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'Document',
		'Production',
		2,	--	ACTION ID For INSERT
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'DocumentNode',
		CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[DocumentNode] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'Title',
		CONVERT(nvarchar(4000), NEW.[Title], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[Title] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'Owner',
		CONVERT(nvarchar(4000), NEW.[Owner], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[Owner] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'FolderFlag',
		CONVERT(nvarchar(4000), NEW.[FolderFlag], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[FolderFlag] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'FileName',
		CONVERT(nvarchar(4000), NEW.[FileName], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[FileName] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'FileExtension',
		CONVERT(nvarchar(4000), NEW.[FileExtension], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[FileExtension] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'Revision',
		CONVERT(nvarchar(4000), NEW.[Revision], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[Revision] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'ChangeNumber',
		CONVERT(nvarchar(4000), NEW.[ChangeNumber], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[ChangeNumber] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'Status',
		CONVERT(nvarchar(4000), NEW.[Status], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[Status] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'DocumentSummary',
		CONVERT(nvarchar(4000), NEW.[DocumentSummary], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[DocumentSummary] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'Document',
		dbo.AUDIT_fn_HexToStr(NEW.[Document]),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[Document] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'rowguid',
		CONVERT(nvarchar(4000), NEW.[rowguid], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[rowguid] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])), '''', '''''')+'''', '[DocumentNode] Is Null')),
		'ModifiedDate',
		CONVERT(nvarchar(4000), NEW.[ModifiedDate], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])))
	FROM inserted NEW
	WHERE NEW.[ModifiedDate] Is Not Null
    

	-- Lookup 
	

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  

End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[Production].[tr_i_AUDIT_Document]', @order='Last', @stmttype='INSERT'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
