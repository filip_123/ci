/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.06
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_u_AUDIT_Document


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [Production].[tr_u_AUDIT_Document]
Print 'Create Trigger [Production].[tr_u_AUDIT_Document]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [Production].[tr_u_AUDIT_Document]
ON [Production].[Document]
FOR UPDATE

	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  UPDATE TRIGGER for Table:  [Production].[Document]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.13
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AUDIT_LOG_TRANSACTION_ID	Int,
		@PRIM_KEY			nvarchar(4000),
		@Inserted	    		bit,
		--@TABLE_NAME				nvarchar(4000),
 		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[Production].[Document]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'Document',
		'Production',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	SET @Inserted = 0
	
	If UPDATE([DocumentNode])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'DocumentNode',
			CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])),
			CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode])),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			WHERE (
			
			
				(
					CONVERT(nvarchar(max), NEW.[DocumentNode]) <>
					CONVERT(nvarchar(max), OLD.[DocumentNode])
				) Or
			
				(
					NEW.[DocumentNode] Is Null And
					OLD.[DocumentNode] Is Not Null
				) Or
				(
					NEW.[DocumentNode] Is Not Null And
					OLD.[DocumentNode] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Title])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'Title',
			CONVERT(nvarchar(4000), OLD.[Title], 0),
			CONVERT(nvarchar(4000), NEW.[Title], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[Title] <>
					OLD.[Title]
				) Or
			
				(
					NEW.[Title] Is Null And
					OLD.[Title] Is Not Null
				) Or
				(
					NEW.[Title] Is Not Null And
					OLD.[Title] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Owner])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'Owner',
			CONVERT(nvarchar(4000), OLD.[Owner], 0),
			CONVERT(nvarchar(4000), NEW.[Owner], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[Owner] <>
					OLD.[Owner]
				) Or
			
				(
					NEW.[Owner] Is Null And
					OLD.[Owner] Is Not Null
				) Or
				(
					NEW.[Owner] Is Not Null And
					OLD.[Owner] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([FolderFlag])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'FolderFlag',
			CONVERT(nvarchar(4000), OLD.[FolderFlag], 0),
			CONVERT(nvarchar(4000), NEW.[FolderFlag], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[FolderFlag] <>
					OLD.[FolderFlag]
				) Or
			
				(
					NEW.[FolderFlag] Is Null And
					OLD.[FolderFlag] Is Not Null
				) Or
				(
					NEW.[FolderFlag] Is Not Null And
					OLD.[FolderFlag] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([FileName])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'FileName',
			CONVERT(nvarchar(4000), OLD.[FileName], 0),
			CONVERT(nvarchar(4000), NEW.[FileName], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[FileName] <>
					OLD.[FileName]
				) Or
			
				(
					NEW.[FileName] Is Null And
					OLD.[FileName] Is Not Null
				) Or
				(
					NEW.[FileName] Is Not Null And
					OLD.[FileName] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([FileExtension])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'FileExtension',
			CONVERT(nvarchar(4000), OLD.[FileExtension], 0),
			CONVERT(nvarchar(4000), NEW.[FileExtension], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[FileExtension] <>
					OLD.[FileExtension]
				) Or
			
				(
					NEW.[FileExtension] Is Null And
					OLD.[FileExtension] Is Not Null
				) Or
				(
					NEW.[FileExtension] Is Not Null And
					OLD.[FileExtension] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Revision])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'Revision',
			CONVERT(nvarchar(4000), OLD.[Revision], 0),
			CONVERT(nvarchar(4000), NEW.[Revision], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[Revision] <>
					OLD.[Revision]
				) Or
			
				(
					NEW.[Revision] Is Null And
					OLD.[Revision] Is Not Null
				) Or
				(
					NEW.[Revision] Is Not Null And
					OLD.[Revision] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ChangeNumber])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'ChangeNumber',
			CONVERT(nvarchar(4000), OLD.[ChangeNumber], 0),
			CONVERT(nvarchar(4000), NEW.[ChangeNumber], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[ChangeNumber] <>
					OLD.[ChangeNumber]
				) Or
			
				(
					NEW.[ChangeNumber] Is Null And
					OLD.[ChangeNumber] Is Not Null
				) Or
				(
					NEW.[ChangeNumber] Is Not Null And
					OLD.[ChangeNumber] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Status])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'Status',
			CONVERT(nvarchar(4000), OLD.[Status], 0),
			CONVERT(nvarchar(4000), NEW.[Status], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[Status] <>
					OLD.[Status]
				) Or
			
				(
					NEW.[Status] Is Null And
					OLD.[Status] Is Not Null
				) Or
				(
					NEW.[Status] Is Not Null And
					OLD.[Status] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([DocumentSummary])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'DocumentSummary',
			CONVERT(nvarchar(4000), OLD.[DocumentSummary], 0),
			CONVERT(nvarchar(4000), NEW.[DocumentSummary], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[DocumentSummary] <>
					OLD.[DocumentSummary]
				) Or
			
				(
					NEW.[DocumentSummary] Is Null And
					OLD.[DocumentSummary] Is Not Null
				) Or
				(
					NEW.[DocumentSummary] Is Not Null And
					OLD.[DocumentSummary] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Document])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'Document',
			dbo.AUDIT_fn_HexToStr(OLD.[Document]),
			dbo.AUDIT_fn_HexToStr(NEW.[Document]),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[Document] <>
					OLD.[Document]
				) Or
			
				(
					NEW.[Document] Is Null And
					OLD.[Document] Is Not Null
				) Or
				(
					NEW.[Document] Is Not Null And
					OLD.[Document] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([rowguid])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'rowguid',
			CONVERT(nvarchar(4000), OLD.[rowguid], 0),
			CONVERT(nvarchar(4000), NEW.[rowguid], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[rowguid] <>
					OLD.[rowguid]
				) Or
			
				(
					NEW.[rowguid] Is Null And
					OLD.[rowguid] Is Not Null
				) Or
				(
					NEW.[rowguid] Is Not Null And
					OLD.[rowguid] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ModifiedDate])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DocumentNode]=N'''+replace(CONVERT(nvarchar(max), CONVERT(nvarchar(max), IsNull(Convert(nvarchar(max), OLD.[DocumentNode]), Convert(nvarchar(max), NEW.[DocumentNode])) )),'''' ,'''''')+'''', '[DocumentNode] Is Null')),
		    'ModifiedDate',
			CONVERT(nvarchar(4000), OLD.[ModifiedDate], 121),
			CONVERT(nvarchar(4000), NEW.[ModifiedDate], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode]))), CONVERT(nvarchar(500), CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[DocumentNode]))=CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[DocumentNode])) or (NEW.[DocumentNode] Is Null and OLD.[DocumentNode] Is Null))
			where (
			
			
				(
					NEW.[ModifiedDate] <>
					OLD.[ModifiedDate]
				) Or
			
				(
					NEW.[ModifiedDate] Is Null And
					OLD.[ModifiedDate] Is Not Null
				) Or
				(
					NEW.[ModifiedDate] Is Not Null And
					OLD.[ModifiedDate] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
	END

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  
End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[Production].[tr_u_AUDIT_Document]', @order='Last', @stmttype='UPDATE'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
