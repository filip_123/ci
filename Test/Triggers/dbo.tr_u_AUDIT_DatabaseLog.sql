/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.06
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_u_AUDIT_DatabaseLog


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [dbo].[tr_u_AUDIT_DatabaseLog]
Print 'Create Trigger [dbo].[tr_u_AUDIT_DatabaseLog]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [dbo].[tr_u_AUDIT_DatabaseLog]
ON [dbo].[DatabaseLog]
FOR UPDATE

	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  UPDATE TRIGGER for Table:  [dbo].[DatabaseLog]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.13
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AUDIT_LOG_TRANSACTION_ID	Int,
		@PRIM_KEY			nvarchar(4000),
		@Inserted	    		bit,
		--@TABLE_NAME				nvarchar(4000),
 		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[dbo].[DatabaseLog]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'DatabaseLog',
		'dbo',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	SET @Inserted = 0
	
	If UPDATE([DatabaseLogID])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'DatabaseLogID',
			CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0),
			CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			WHERE (
			
			
				(
					NEW.[DatabaseLogID] <>
					OLD.[DatabaseLogID]
				) Or
			
				(
					NEW.[DatabaseLogID] Is Null And
					OLD.[DatabaseLogID] Is Not Null
				) Or
				(
					NEW.[DatabaseLogID] Is Not Null And
					OLD.[DatabaseLogID] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([PostTime])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'PostTime',
			CONVERT(nvarchar(4000), OLD.[PostTime], 121),
			CONVERT(nvarchar(4000), NEW.[PostTime], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					NEW.[PostTime] <>
					OLD.[PostTime]
				) Or
			
				(
					NEW.[PostTime] Is Null And
					OLD.[PostTime] Is Not Null
				) Or
				(
					NEW.[PostTime] Is Not Null And
					OLD.[PostTime] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([DatabaseUser])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'DatabaseUser',
			CONVERT(nvarchar(4000), OLD.[DatabaseUser], 0),
			CONVERT(nvarchar(4000), NEW.[DatabaseUser], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					NEW.[DatabaseUser] <>
					OLD.[DatabaseUser]
				) Or
			
				(
					NEW.[DatabaseUser] Is Null And
					OLD.[DatabaseUser] Is Not Null
				) Or
				(
					NEW.[DatabaseUser] Is Not Null And
					OLD.[DatabaseUser] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Event])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'Event',
			CONVERT(nvarchar(4000), OLD.[Event], 0),
			CONVERT(nvarchar(4000), NEW.[Event], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					NEW.[Event] <>
					OLD.[Event]
				) Or
			
				(
					NEW.[Event] Is Null And
					OLD.[Event] Is Not Null
				) Or
				(
					NEW.[Event] Is Not Null And
					OLD.[Event] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Schema])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'Schema',
			CONVERT(nvarchar(4000), OLD.[Schema], 0),
			CONVERT(nvarchar(4000), NEW.[Schema], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					NEW.[Schema] <>
					OLD.[Schema]
				) Or
			
				(
					NEW.[Schema] Is Null And
					OLD.[Schema] Is Not Null
				) Or
				(
					NEW.[Schema] Is Not Null And
					OLD.[Schema] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Object])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'Object',
			CONVERT(nvarchar(4000), OLD.[Object], 0),
			CONVERT(nvarchar(4000), NEW.[Object], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					NEW.[Object] <>
					OLD.[Object]
				) Or
			
				(
					NEW.[Object] Is Null And
					OLD.[Object] Is Not Null
				) Or
				(
					NEW.[Object] Is Not Null And
					OLD.[Object] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TSQL])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'TSQL',
			CONVERT(nvarchar(4000), OLD.[TSQL], 0),
			CONVERT(nvarchar(4000), NEW.[TSQL], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					NEW.[TSQL] <>
					OLD.[TSQL]
				) Or
			
				(
					NEW.[TSQL] Is Null And
					OLD.[TSQL] Is Not Null
				) Or
				(
					NEW.[TSQL] Is Not Null And
					OLD.[TSQL] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([XmlEvent])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[DatabaseLogID]='+CONVERT(nvarchar(4000), IsNull(OLD.[DatabaseLogID], NEW.[DatabaseLogID]), 0), '[DatabaseLogID] Is Null')),
		    'XmlEvent',
			CONVERT(nvarchar(max), CONVERT(nvarchar(max), OLD.[XmlEvent])),
			CONVERT(nvarchar(max), CONVERT(nvarchar(max), NEW.[XmlEvent])),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[DatabaseLogID], 0)=CONVERT(nvarchar(4000), OLD.[DatabaseLogID], 0) or (NEW.[DatabaseLogID] Is Null and OLD.[DatabaseLogID] Is Null))
			where (
			
			
				(
					CONVERT(nvarchar(max), NEW.[XmlEvent]) <>
					CONVERT(nvarchar(max), OLD.[XmlEvent])
				) Or
			
				(
					NEW.[XmlEvent] Is Null And
					OLD.[XmlEvent] Is Not Null
				) Or
				(
					NEW.[XmlEvent] Is Not Null And
					OLD.[XmlEvent] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
	END

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  
End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[dbo].[tr_u_AUDIT_DatabaseLog]', @order='Last', @stmttype='UPDATE'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
