/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_i_AUDIT_Private_AssertEqualsTableSchema_Actual


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [tSQLt].[tr_i_AUDIT_Private_AssertEqualsTableSchema_Actual]
Print 'Create Trigger [tSQLt].[tr_i_AUDIT_Private_AssertEqualsTableSchema_Actual]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [tSQLt].[tr_i_AUDIT_Private_AssertEqualsTableSchema_Actual]
ON [tSQLt].[Private_AssertEqualsTableSchema_Actual]
FOR INSERT


	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  INSERT TRIGGER for Table:  [tSQLt].[Private_AssertEqualsTableSchema_Actual]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.15
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE				varchar(50),
		@AUDIT_LOG_TRANSACTION_ID		Int,
		@PRIM_KEY				nvarchar(4000),
		--@TABLE_NAME				nvarchar(4000),
		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[tSQLt].[Private_AssertEqualsTableSchema_Actual]'
	Select @ROWS_COUNT=count(*) from inserted
	Set @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'Private_AssertEqualsTableSchema_Actual',
		'tSQLt',
		2,	--	ACTION ID For INSERT
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'name',
		CONVERT(nvarchar(4000), NEW.[name], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[name] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'RANK(column_id)',
		CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[RANK(column_id)] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'system_type_id',
		CONVERT(nvarchar(4000), NEW.[system_type_id], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[system_type_id] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'user_type_id',
		CONVERT(nvarchar(4000), NEW.[user_type_id], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[user_type_id] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'max_length',
		CONVERT(nvarchar(4000), NEW.[max_length], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[max_length] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'precision',
		CONVERT(nvarchar(4000), NEW.[precision], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[precision] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'scale',
		CONVERT(nvarchar(4000), NEW.[scale], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[scale] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'collation_name',
		CONVERT(nvarchar(4000), NEW.[collation_name], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[collation_name] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'is_nullable',
		CONVERT(nvarchar(4000), NEW.[is_nullable], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[is_nullable] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3, KEY4
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[name], 0), '''', '''''')+'''', '[name] Is Null') + ' AND ' + IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0), '[RANK(column_id)] Is Null') + ' AND ' + IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[system_type_id], 0), '''', '''''')+'''', '[system_type_id] Is Null') + ' AND ' + IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), NEW.[user_type_id], 0), '''', '''''')+'''', '[user_type_id] Is Null') + ' AND ' + IsNull('[max_length]='+CONVERT(nvarchar(4000), NEW.[max_length], 0), '[max_length] Is Null') + ' AND ' + IsNull('[precision]='+CONVERT(nvarchar(4000), NEW.[precision], 0), '[precision] Is Null') + ' AND ' + IsNull('[scale]='+CONVERT(nvarchar(4000), NEW.[scale], 0), '[scale] Is Null') + ' AND ' + IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), NEW.[collation_name], 0), '''', '''''')+'''', '[collation_name] Is Null') + ' AND ' + IsNull('[is_nullable]='+CONVERT(nvarchar(4000), NEW.[is_nullable], 0), '[is_nullable] Is Null') + ' AND ' + IsNull('[is_identity]='+CONVERT(nvarchar(4000), NEW.[is_identity], 0), '[is_identity] Is Null')),
		'is_identity',
		CONVERT(nvarchar(4000), NEW.[is_identity], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0))
	FROM inserted NEW
	WHERE NEW.[is_identity] Is Not Null
    

	-- Lookup 
	

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  

End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[tSQLt].[tr_i_AUDIT_Private_AssertEqualsTableSchema_Actual]', @order='Last', @stmttype='INSERT'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
