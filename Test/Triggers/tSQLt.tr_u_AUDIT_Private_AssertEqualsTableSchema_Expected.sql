/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0002
DATE:      01-10-2017 12.59.05
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_u_AUDIT_Private_AssertEqualsTableSchema_Expected


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [tSQLt].[tr_u_AUDIT_Private_AssertEqualsTableSchema_Expected]
Print 'Create Trigger [tSQLt].[tr_u_AUDIT_Private_AssertEqualsTableSchema_Expected]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [tSQLt].[tr_u_AUDIT_Private_AssertEqualsTableSchema_Expected]
ON [tSQLt].[Private_AssertEqualsTableSchema_Expected]
FOR UPDATE

	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  UPDATE TRIGGER for Table:  [tSQLt].[Private_AssertEqualsTableSchema_Expected]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.15
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AUDIT_LOG_TRANSACTION_ID	Int,
		@PRIM_KEY			nvarchar(4000),
		@Inserted	    		bit,
		--@TABLE_NAME				nvarchar(4000),
 		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[tSQLt].[Private_AssertEqualsTableSchema_Expected]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'Private_AssertEqualsTableSchema_Expected',
		'tSQLt',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	SET @Inserted = 0
	
	If UPDATE([name])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'name',
			CONVERT(nvarchar(4000), OLD.[name], 0),
			CONVERT(nvarchar(4000), NEW.[name], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[name] <>
					OLD.[name]
				) Or
			
				(
					NEW.[name] Is Null And
					OLD.[name] Is Not Null
				) Or
				(
					NEW.[name] Is Not Null And
					OLD.[name] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([RANK(column_id)])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'RANK(column_id)',
			CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0),
			CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[RANK(column_id)] <>
					OLD.[RANK(column_id)]
				) Or
			
				(
					NEW.[RANK(column_id)] Is Null And
					OLD.[RANK(column_id)] Is Not Null
				) Or
				(
					NEW.[RANK(column_id)] Is Not Null And
					OLD.[RANK(column_id)] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([system_type_id])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'system_type_id',
			CONVERT(nvarchar(4000), OLD.[system_type_id], 0),
			CONVERT(nvarchar(4000), NEW.[system_type_id], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[system_type_id] <>
					OLD.[system_type_id]
				) Or
			
				(
					NEW.[system_type_id] Is Null And
					OLD.[system_type_id] Is Not Null
				) Or
				(
					NEW.[system_type_id] Is Not Null And
					OLD.[system_type_id] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([user_type_id])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'user_type_id',
			CONVERT(nvarchar(4000), OLD.[user_type_id], 0),
			CONVERT(nvarchar(4000), NEW.[user_type_id], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[user_type_id] <>
					OLD.[user_type_id]
				) Or
			
				(
					NEW.[user_type_id] Is Null And
					OLD.[user_type_id] Is Not Null
				) Or
				(
					NEW.[user_type_id] Is Not Null And
					OLD.[user_type_id] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([max_length])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'max_length',
			CONVERT(nvarchar(4000), OLD.[max_length], 0),
			CONVERT(nvarchar(4000), NEW.[max_length], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[max_length] <>
					OLD.[max_length]
				) Or
			
				(
					NEW.[max_length] Is Null And
					OLD.[max_length] Is Not Null
				) Or
				(
					NEW.[max_length] Is Not Null And
					OLD.[max_length] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([precision])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'precision',
			CONVERT(nvarchar(4000), OLD.[precision], 0),
			CONVERT(nvarchar(4000), NEW.[precision], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[precision] <>
					OLD.[precision]
				) Or
			
				(
					NEW.[precision] Is Null And
					OLD.[precision] Is Not Null
				) Or
				(
					NEW.[precision] Is Not Null And
					OLD.[precision] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([scale])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'scale',
			CONVERT(nvarchar(4000), OLD.[scale], 0),
			CONVERT(nvarchar(4000), NEW.[scale], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[scale] <>
					OLD.[scale]
				) Or
			
				(
					NEW.[scale] Is Null And
					OLD.[scale] Is Not Null
				) Or
				(
					NEW.[scale] Is Not Null And
					OLD.[scale] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([collation_name])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'collation_name',
			CONVERT(nvarchar(4000), OLD.[collation_name], 0),
			CONVERT(nvarchar(4000), NEW.[collation_name], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[collation_name] <>
					OLD.[collation_name]
				) Or
			
				(
					NEW.[collation_name] Is Null And
					OLD.[collation_name] Is Not Null
				) Or
				(
					NEW.[collation_name] Is Not Null And
					OLD.[collation_name] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([is_nullable])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'is_nullable',
			CONVERT(nvarchar(4000), OLD.[is_nullable], 0),
			CONVERT(nvarchar(4000), NEW.[is_nullable], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[is_nullable] <>
					OLD.[is_nullable]
				) Or
			
				(
					NEW.[is_nullable] Is Null And
					OLD.[is_nullable] Is Not Null
				) Or
				(
					NEW.[is_nullable] Is Not Null And
					OLD.[is_nullable] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([is_identity])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3, KEY4
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[name], NEW.[name]), 0),'''' ,'''''')+'''', '[name] Is Null')+' AND '+IsNull('[RANK(column_id)]='+CONVERT(nvarchar(4000), IsNull(OLD.[RANK(column_id)], NEW.[RANK(column_id)]), 0), '[RANK(column_id)] Is Null')+' AND '+IsNull('[system_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[system_type_id], NEW.[system_type_id]), 0),'''' ,'''''')+'''', '[system_type_id] Is Null')+' AND '+IsNull('[user_type_id]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[user_type_id], NEW.[user_type_id]), 0),'''' ,'''''')+'''', '[user_type_id] Is Null')+' AND '+IsNull('[max_length]='+CONVERT(nvarchar(4000), IsNull(OLD.[max_length], NEW.[max_length]), 0), '[max_length] Is Null')+' AND '+IsNull('[precision]='+CONVERT(nvarchar(4000), IsNull(OLD.[precision], NEW.[precision]), 0), '[precision] Is Null')+' AND '+IsNull('[scale]='+CONVERT(nvarchar(4000), IsNull(OLD.[scale], NEW.[scale]), 0), '[scale] Is Null')+' AND '+IsNull('[collation_name]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[collation_name], NEW.[collation_name]), 0),'''' ,'''''')+'''', '[collation_name] Is Null')+' AND '+IsNull('[is_nullable]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_nullable], NEW.[is_nullable]), 0), '[is_nullable] Is Null')+' AND '+IsNull('[is_identity]='+CONVERT(nvarchar(4000), IsNull(OLD.[is_identity], NEW.[is_identity]), 0), '[is_identity] Is Null')),
		    'is_identity',
			CONVERT(nvarchar(4000), OLD.[is_identity], 0),
			CONVERT(nvarchar(4000), NEW.[is_identity], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[name], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[name], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[system_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[system_type_id], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[user_type_id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[user_type_id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[name], 0)=CONVERT(nvarchar(4000), OLD.[name], 0) or (NEW.[name] Is Null and OLD.[name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[RANK(column_id)], 0)=CONVERT(nvarchar(4000), OLD.[RANK(column_id)], 0) or (NEW.[RANK(column_id)] Is Null and OLD.[RANK(column_id)] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[system_type_id], 0)=CONVERT(nvarchar(4000), OLD.[system_type_id], 0) or (NEW.[system_type_id] Is Null and OLD.[system_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[user_type_id], 0)=CONVERT(nvarchar(4000), OLD.[user_type_id], 0) or (NEW.[user_type_id] Is Null and OLD.[user_type_id] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[max_length], 0)=CONVERT(nvarchar(4000), OLD.[max_length], 0) or (NEW.[max_length] Is Null and OLD.[max_length] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[precision], 0)=CONVERT(nvarchar(4000), OLD.[precision], 0) or (NEW.[precision] Is Null and OLD.[precision] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[scale], 0)=CONVERT(nvarchar(4000), OLD.[scale], 0) or (NEW.[scale] Is Null and OLD.[scale] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[collation_name], 0)=CONVERT(nvarchar(4000), OLD.[collation_name], 0) or (NEW.[collation_name] Is Null and OLD.[collation_name] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_nullable], 0)=CONVERT(nvarchar(4000), OLD.[is_nullable], 0) or (NEW.[is_nullable] Is Null and OLD.[is_nullable] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[is_identity], 0)=CONVERT(nvarchar(4000), OLD.[is_identity], 0) or (NEW.[is_identity] Is Null and OLD.[is_identity] Is Null))
			WHERE (
			
			
				(
					NEW.[is_identity] <>
					OLD.[is_identity]
				) Or
			
				(
					NEW.[is_identity] Is Null And
					OLD.[is_identity] Is Not Null
				) Or
				(
					NEW.[is_identity] Is Not Null And
					OLD.[is_identity] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
	END

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  
End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[tSQLt].[tr_u_AUDIT_Private_AssertEqualsTableSchema_Expected]', @order='Last', @stmttype='UPDATE'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
