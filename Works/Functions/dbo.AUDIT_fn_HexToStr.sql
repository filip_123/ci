/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Functions:  AUDIT_fn_HexToStr


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Function [dbo].[AUDIT_fn_HexToStr]
Print 'Create Function [dbo].[AUDIT_fn_HexToStr]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

create function [dbo].[AUDIT_fn_HexToStr](@hex varbinary(8000))
returns varchar(8000)
as
begin
declare 
	@len int,
	@counter int,
	@res varchar(8000),
	@string char(16),
	@byte binary(1)
	set @string = '0123456789ABCDEF'
	set @res = '0x'
	set @len = datalength(@hex)
	set @counter = 1
	while(@counter <= @len)
	begin
		set @byte = substring(@hex, @counter, 1)
		set @res = @res + substring(@string, 1 + @byte/16, 1) + substring(@string, 1 + @byte - (@byte/16)*16, 1)
		set @counter = @counter + 1
	end
	return @res
end
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
