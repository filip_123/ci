/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Functions:  AUDIT_fn_SqlVariantToString


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Function [dbo].[AUDIT_fn_SqlVariantToString]
Print 'Create Function [dbo].[AUDIT_fn_SqlVariantToString]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

create function dbo.AUDIT_fn_SqlVariantToString(@var sql_variant, @string bit)
returns nvarchar(4000)
as
begin
declare
@type varchar(20),
@result nvarchar(4000)
set @type = cast(SQL_VARIANT_PROPERTY(@var,'BaseType') as varchar(20))
if (@type='binary' or @type='varbinary')
	set @result = cast(dbo.AUDIT_fn_HexToStr(cast(@var as varbinary(8000))) as nvarchar(4000))
else if (@type='float' or @type='real')
	set @result = convert(nvarchar(4000), @var, 3)
else if (@type='int'
	or @type='tinyint'
	or @type='smallint'
	or @type='bigint'
	or @type='bit'
	or @type='decimal'
	or @type='numeric'
	)
	set @result = convert(nvarchar(4000), @var)
else if (@type='timestamp')
	set @result = convert(nvarchar(4000), convert(bigint, @var))
else if (@string = 1)
	set @result = 'N''' + convert(nvarchar(4000), @var) + ''''
else
	set @result = convert(nvarchar(4000), @var)
return @result
end
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
