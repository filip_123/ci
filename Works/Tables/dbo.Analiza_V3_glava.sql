/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Analiza_V3_glava


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Analiza_V3_glava]
Print 'Create Table [dbo].[Analiza_V3_glava]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Analiza_V3_glava] (
		[index_glave]          [int] IDENTITY(1, 1) NOT NULL,
		[stevilka_analize]     [varchar](12) NOT NULL,
		[naziv_opis]           [varchar](512) NULL,
		[xls_datoteka]         [varchar](120) NULL,
		[od_sm]                [varchar](6) NULL,
		[do_sm]                [varchar](6) NULL,
		[od_projekt]           [int] NULL,
		[do_projekt]           [int] NULL,
		[leto_od]              [smallint] NOT NULL,
		[leto_do]              [smallint] NOT NULL,
		[mesec_od]             [smallint] NOT NULL,
		[mesec_do]             [smallint] NOT NULL,
		[grupa_sm]             [bit] NOT NULL,
		[grupa_projekt]        [bit] NOT NULL,
		[grupa_leto_mesec]     [bit] NOT NULL,
		[vnos_plana]           [bit] NOT NULL,
		CONSTRAINT [PK_Analiza_V3_glava]
		PRIMARY KEY
		NONCLUSTERED
		([index_glave])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	ADD
	CONSTRAINT [DF_Analiza_V3_glava_grupa_leto_mesec]
	DEFAULT ((0)) FOR [grupa_leto_mesec]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	ADD
	CONSTRAINT [DF_Analiza_V3_glava_grupa_projekt]
	DEFAULT ((0)) FOR [grupa_projekt]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	ADD
	CONSTRAINT [DF_Analiza_V3_glava_grupa_sm]
	DEFAULT ((0)) FOR [grupa_sm]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	ADD
	CONSTRAINT [DF_Analiza_V3_glava_vnos_plana]
	DEFAULT ((1)) FOR [vnos_plana]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_V3_glava_Obdobje]
	FOREIGN KEY ([leto_od], [mesec_od]) REFERENCES [dbo].[Obdobje] ([leto], [mesec])
ALTER TABLE [dbo].[Analiza_V3_glava]
	CHECK CONSTRAINT [FK_Analiza_V3_glava_Obdobje]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_V3_glava_Obdobje1]
	FOREIGN KEY ([leto_do], [mesec_do]) REFERENCES [dbo].[Obdobje] ([leto], [mesec])
ALTER TABLE [dbo].[Analiza_V3_glava]
	CHECK CONSTRAINT [FK_Analiza_V3_glava_Obdobje1]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_V3_glava_Projekt]
	FOREIGN KEY ([od_projekt]) REFERENCES [dbo].[Projekt] ([sifra])
ALTER TABLE [dbo].[Analiza_V3_glava]
	CHECK CONSTRAINT [FK_Analiza_V3_glava_Projekt]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_V3_glava_Projekt1]
	FOREIGN KEY ([do_projekt]) REFERENCES [dbo].[Projekt] ([sifra])
ALTER TABLE [dbo].[Analiza_V3_glava]
	CHECK CONSTRAINT [FK_Analiza_V3_glava_Projekt1]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_V3_glava_Stroskovno_mesto]
	FOREIGN KEY ([od_sm]) REFERENCES [dbo].[Stroskovno_mesto] ([sifra])
ALTER TABLE [dbo].[Analiza_V3_glava]
	CHECK CONSTRAINT [FK_Analiza_V3_glava_Stroskovno_mesto]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_V3_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_V3_glava_Stroskovno_mesto1]
	FOREIGN KEY ([do_sm]) REFERENCES [dbo].[Stroskovno_mesto] ([sifra])
ALTER TABLE [dbo].[Analiza_V3_glava]
	CHECK CONSTRAINT [FK_Analiza_V3_glava_Stroskovno_mesto1]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
