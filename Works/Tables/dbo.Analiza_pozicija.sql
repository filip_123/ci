/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Analiza_pozicija


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Analiza_pozicija]
Print 'Create Table [dbo].[Analiza_pozicija]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Analiza_pozicija] (
		[index_glave]     [int] NOT NULL,
		[sifra_aop]       [char](4) NOT NULL,
		[gre_v_izpis]     [bit] NOT NULL,
		[naziv]           [varchar](200) NULL,
		[konto]           [dbo].[konti] NULL,
		[tip_aop]         [char](3) NULL,
		[formula]         [varchar](512) NULL,
		CONSTRAINT [PK_Analiza_pozicija]
		PRIMARY KEY
		NONCLUSTERED
		([index_glave], [sifra_aop])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_pozicija]
	ADD
	CONSTRAINT [DF_Analiza_pozicija_gre_v_izpis]
	DEFAULT ((0)) FOR [gre_v_izpis]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_pozicija] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_pozicija]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_pozicija_Analiza_glava]
	FOREIGN KEY ([index_glave]) REFERENCES [dbo].[Analiza_glava] ([index_glave])
ALTER TABLE [dbo].[Analiza_pozicija]
	CHECK CONSTRAINT [FK_Analiza_pozicija_Analiza_glava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Analiza_pozicija]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Analiza_pozicija_Kontni_plan]
	FOREIGN KEY ([konto]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Analiza_pozicija]
	CHECK CONSTRAINT [FK_Analiza_pozicija_Kontni_plan]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
