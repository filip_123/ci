/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Banka


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Banka]
Print 'Create Table [dbo].[Banka]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Banka] (
		[sifra]                   [varchar](6) NOT NULL,
		[opis]                    [varchar](30) NOT NULL,
		[naziv]                   [varchar](50) NULL,
		[naslov_1]                [varchar](50) NULL,
		[naslov_2]                [varchar](50) NULL,
		[naslov_3]                [varchar](50) NULL,
		[posta]                   [varchar](10) NULL,
		[drzava]                  [varchar](6) NULL,
		[telefon]                 [varchar](40) NULL,
		[fax]                     [varchar](40) NULL,
		[racun]                   [varchar](50) NULL,
		[swift]                   [varchar](11) NULL,
		[referenca]               [varchar](15) NULL,
		[posebno_navodilo_1]      [varchar](35) NULL,
		[posebno_navodilo_2]      [varchar](35) NULL,
		[sifra_valute]            [varchar](6) NULL,
		[opomba]                  [varchar](50) NULL,
		[ima_ziro_racun]          [bit] NOT NULL,
		[ziro_racun]              [varchar](18) NULL,
		[kraj]                    [varchar](10) NULL,
		[posrednica_naziv]        [varchar](50) NULL,
		[posrednica_naslov_1]     [varchar](50) NULL,
		[posrednica_naslov_2]     [varchar](50) NULL,
		[posrednica_naslov_3]     [varchar](50) NULL,
		[posrednica_kraj]         [varchar](10) NULL,
		[posrednica_swift]        [varchar](11) NULL,
		[posrednica_racun]        [varchar](50) NULL,
		[posrednica_drzava]       [varchar](6) NULL,
		CONSTRAINT [PK_Banka]
		PRIMARY KEY
		NONCLUSTERED
		([sifra])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Banka]
	ADD
	CONSTRAINT [DF_Banka_ima_ziro_racun]
	DEFAULT ((0)) FOR [ima_ziro_racun]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Banka] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Banka]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Banka_Drzava]
	FOREIGN KEY ([drzava]) REFERENCES [dbo].[Drzava] ([sifra])
ALTER TABLE [dbo].[Banka]
	CHECK CONSTRAINT [FK_Banka_Drzava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Banka]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Banka_Posta_posta_drzava]
	FOREIGN KEY ([posta], [drzava]) REFERENCES [dbo].[Posta] ([postna_stevilka], [drzava])
ALTER TABLE [dbo].[Banka]
	CHECK CONSTRAINT [FK_Banka_Posta_posta_drzava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Banka]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Banka_sifra_valute]
	FOREIGN KEY ([sifra_valute]) REFERENCES [dbo].[Valuta] ([sifra])
ALTER TABLE [dbo].[Banka]
	CHECK CONSTRAINT [FK_Banka_sifra_valute]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
