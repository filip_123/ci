/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Izhodna_faktura


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Izhodna_faktura]
Print 'Create Table [dbo].[Izhodna_faktura]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Izhodna_faktura] (
		[identity_fakture]                         [int] IDENTITY(1, 1) NOT NULL,
		[interno_podjetje]                         [tinyint] NOT NULL,
		[zaporedna_stevilka]                       [int] NOT NULL,
		[index_zapiranja]                          [int] NULL,
		[leto_temeljnice]                          [smallint] NULL,
		[vrsta_temeljnice]                         [tinyint] NULL,
		[stevilka_temeljnice]                      [smallint] NULL,
		[vrsta_knjizbe]                            [tinyint] NULL,
		[originalni_dokument]                      [varchar](12) NOT NULL,
		[leto]                                     [smallint] NOT NULL,
		[mesec]                                    [smallint] NOT NULL,
		[poslovni_partner]                         [varchar](7) NOT NULL,
		[opis_komentar]                            [varchar](60) NULL,
		[datum_izstavitve]                         [smalldatetime] NOT NULL,
		[datum_storitve]                           [smalldatetime] NULL,
		[datum_valute]                             [smalldatetime] NOT NULL,
		[znesek]                                   [money] NULL,
		[referent]                                 [tinyint] NULL,
		[sifra_vrste_placila]                      [varchar](6) NULL,
		[stroskovno_mesto]                         [varchar](6) NULL,
		[stroskovni_nosilec]                       [varchar](6) NULL,
		[konto]                                    [dbo].[konti] NOT NULL,
		[projekt]                                  [int] NULL,
		[ecl]                                      [varchar](10) NULL,
		[ecl_datum_prejema]                        [datetime] NULL,
		[ecl_datum_izdaje]                         [datetime] NULL,
		[ecl_carinski_postopek]                    [varchar](6) NULL,
		[banka_tecaj]                              [tinyint] NULL,
		[datum_tecaja]                             [smalldatetime] NULL,
		[sifra_valute]                             [varchar](6) NULL,
		[tip_tecaja]                               [tinyint] NULL,
		[pariteta]                                 [money] NULL,
		[devizni_znesek]                           [money] NULL,
		[vrsta_fakture]                            [smallint] NULL,
		[stevilka_fakture_sm]                      [int] NULL,
		[memo]                                     [text] NULL,
		[tekst_za_kolicino]                        [varchar](50) NULL,
		[kolicina]                                 [varchar](80) NULL,
		[podjetje]                                 [varchar](90) NULL,
		[ddv]                                      [bit] NOT NULL,
		[ddv_obdobje_leto]                         [smallint] NULL,
		[ddv_obdobje_mesec]                        [smallint] NULL,
		[ddv_obracun]                              [int] NULL,
		[ddv_neobdavceno]                          [money] NULL,
		[ddv_izvoz]                                [money] NULL,
		[ddv_izvoz_stat]                           [money] NULL,
		[ddv_druge_oprostitve]                     [money] NULL,
		[ddv_zav_osnova_st1]                       [money] NULL,
		[ddv_zav_znesek_st1]                       [money] NULL,
		[ddv_zav_osnova_st2]                       [money] NULL,
		[ddv_zav_znesek_st2]                       [money] NULL,
		[ddv_kon_osnova_st1]                       [money] NULL,
		[ddv_kon_znesek_st1]                       [money] NULL,
		[ddv_kon_osnova_st2]                       [money] NULL,
		[ddv_kon_znesek_st2]                       [money] NULL,
		[ddv_zav_osnova_st1_lr]                    [money] NULL,
		[ddv_zav_osnova_st2_lr]                    [money] NULL,
		[ddv_kon_osnova_st1_lr]                    [money] NULL,
		[ddv_kon_osnova_st2_lr]                    [money] NULL,
		[interni_komentar]                         [varchar](60) NULL,
		[ddv_osnova]                               [money] NULL,
		[ddv_znesek]                               [money] NULL,
		[ddv_skupina]                              [tinyint] NULL,
		[lastna_identifikacijska_stevilka_id]      [int] NULL,
		[partner_identifikacijska_stevilka_id]     [int] NULL,
		[partner_zavezanec_ddv]                    [bit] NOT NULL,
		[partner_sedez_slovenija]                  [bit] NOT NULL,
		[partner_eu]                               [bit] NOT NULL,
		[datum_ddv]                                [datetime] NULL,
		CONSTRAINT [PK_Izhodna_faktura]
		PRIMARY KEY
		NONCLUSTERED
		([identity_fakture])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	ADD
	CONSTRAINT [DF_Izhodna_faktura_ddv]
	DEFAULT ((1)) FOR [ddv]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	ADD
	CONSTRAINT [DF_Izhodna_faktura_ddv_osnova_uvoz_stat]
	DEFAULT ((0)) FOR [ddv_izvoz_stat]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	ADD
	CONSTRAINT [DF_Izhodna_faktura_interno_podjetje]
	DEFAULT ((1)) FOR [interno_podjetje]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	ADD
	CONSTRAINT [DF_Izhodna_faktura_partner_eu]
	DEFAULT ((0)) FOR [partner_eu]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	ADD
	CONSTRAINT [DF_Izhodna_faktura_partner_sedez_slovenija]
	DEFAULT ((0)) FOR [partner_sedez_slovenija]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	ADD
	CONSTRAINT [DF_Izhodna_faktura_partner_zavezanec_ddv]
	DEFAULT ((0)) FOR [partner_zavezanec_ddv]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_Izh_fakt_zaporedna_stevilka]
	ON [dbo].[Izhodna_faktura] ([zaporedna_stevilka])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_Izhodna_faktura_index_zapiranja]
	ON [dbo].[Izhodna_faktura] ([index_zapiranja])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE UNIQUE NONCLUSTERED INDEX [ZX_Izhodna_faktura_leto_zap_st_vrsta_knj]
	ON [dbo].[Izhodna_faktura] ([leto], [zaporedna_stevilka], [vrsta_knjizbe])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_Izhodna_faktura_originalni_dokument]
	ON [dbo].[Izhodna_faktura] ([originalni_dokument])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izho_fakt_sif_Ident_lastna]
	FOREIGN KEY ([lastna_identifikacijska_stevilka_id]) REFERENCES [dbo].[sif_Identifikacijske_stevilke] ([identifikacijska_stevilka_id])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izho_fakt_sif_Ident_lastna]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izho_fakt_sif_Ident_partner]
	FOREIGN KEY ([partner_identifikacijska_stevilka_id]) REFERENCES [dbo].[sif_Identifikacijske_stevilke] ([identifikacijska_stevilka_id])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izho_fakt_sif_Ident_partner]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Banka_devizni_tecaj]
	FOREIGN KEY ([banka_tecaj]) REFERENCES [dbo].[Banka_devizni_tecaj] ([sifra_banke])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Banka_devizni_tecaj]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Devizni_tecaj]
	FOREIGN KEY ([banka_tecaj], [datum_tecaja], [sifra_valute], [tip_tecaja]) REFERENCES [dbo].[Devizni_tecaj] ([banka_tecaj], [datum_tecaja], [sifra_valute], [tip_tecaja])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Devizni_tecaj]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Kontni_plan]
	FOREIGN KEY ([konto]) REFERENCES [dbo].[Kontni_plan] ([konto])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Kontni_plan]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Obdobje]
	FOREIGN KEY ([leto], [mesec]) REFERENCES [dbo].[Obdobje] ([leto], [mesec])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Obdobje]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Obdobje_ddv]
	FOREIGN KEY ([ddv_obdobje_leto], [ddv_obdobje_mesec]) REFERENCES [dbo].[Obdobje] ([leto], [mesec])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Obdobje_ddv]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Poslovni_partner]
	FOREIGN KEY ([poslovni_partner]) REFERENCES [dbo].[Poslovni_partner] ([sifra])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Poslovni_partner]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Projekt]
	FOREIGN KEY ([projekt]) REFERENCES [dbo].[Projekt] ([sifra])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Projekt]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Referent]
	FOREIGN KEY ([referent]) REFERENCES [dbo].[Referent] ([referent])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Referent]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Stroskovni_nosilec]
	FOREIGN KEY ([stroskovni_nosilec]) REFERENCES [dbo].[Stroskovni_nosilec] ([sifra])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Stroskovni_nosilec]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Stroskovno_mesto]
	FOREIGN KEY ([stroskovno_mesto]) REFERENCES [dbo].[Stroskovno_mesto] ([sifra])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Stroskovno_mesto]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Temeljnice_glave]
	FOREIGN KEY ([leto_temeljnice], [vrsta_temeljnice], [stevilka_temeljnice]) REFERENCES [dbo].[Temeljnice_glave] ([leto], [vrsta_temeljnice_si], [stevilka_temeljnice])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Temeljnice_glave]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Tip_tecaja]
	FOREIGN KEY ([tip_tecaja]) REFERENCES [dbo].[Tip_tecaja] ([tip])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Tip_tecaja]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Valuta]
	FOREIGN KEY ([sifra_valute]) REFERENCES [dbo].[Valuta] ([sifra])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Valuta]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Vrsta_fakture]
	FOREIGN KEY ([vrsta_fakture]) REFERENCES [dbo].[Vrsta_fakture] ([sifra_vrste_fakture])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Vrsta_fakture]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Vrsta_knjizbe]
	FOREIGN KEY ([vrsta_knjizbe]) REFERENCES [dbo].[Vrsta_knjizbe] ([vrsta])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Vrsta_knjizbe]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Izhodna_faktura]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Izhodna_faktura_Vrsta_placila]
	FOREIGN KEY ([sifra_vrste_placila]) REFERENCES [dbo].[Vrsta_placila] ([sifra_vrste_placila])
ALTER TABLE [dbo].[Izhodna_faktura]
	CHECK CONSTRAINT [FK_Izhodna_faktura_Vrsta_placila]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
