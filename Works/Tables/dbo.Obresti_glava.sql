/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Obresti_glava


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Obresti_glava]
Print 'Create Table [dbo].[Obresti_glava]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Obresti_glava] (
		[leto]                      [int] NOT NULL,
		[stevilka]                  [int] NOT NULL,
		[vrsta_obracuna]            [tinyint] NOT NULL,
		[od_kupca_abc]              [varchar](7) NULL,
		[do_kupca_abc]              [varchar](7) NULL,
		[specifikacija]             [bit] NOT NULL,
		[od_datuma_placila]         [smalldatetime] NULL,
		[do_datuma_placila]         [smalldatetime] NULL,
		[mejni_znesek]              [money] NOT NULL,
		[datum_izracuna]            [smalldatetime] NULL,
		[zakljucen]                 [bit] NOT NULL,
		[knjizen]                   [bit] NOT NULL,
		[devizna_obrestna_mera]     [float] NULL,
		CONSTRAINT [PK_Obresti_glava]
		PRIMARY KEY
		NONCLUSTERED
		([leto], [stevilka])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	ADD
	CONSTRAINT [DF_Obresti_glava_knjizen]
	DEFAULT ((0)) FOR [knjizen]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	ADD
	CONSTRAINT [DF_Obresti_glava_mejni znesek]
	DEFAULT ((0)) FOR [mejni_znesek]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	ADD
	CONSTRAINT [DF_Obresti_glava_specifikacija]
	DEFAULT ((0)) FOR [specifikacija]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	ADD
	CONSTRAINT [DF_Obresti_glava_vrsta_obracuna]
	DEFAULT ((1)) FOR [vrsta_obracuna]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	ADD
	CONSTRAINT [DF_Obresti_glava_zakljucen]
	DEFAULT ((0)) FOR [zakljucen]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Obresti_glava_Poslovni_partner]
	FOREIGN KEY ([od_kupca_abc]) REFERENCES [dbo].[Poslovni_partner] ([sifra])
ALTER TABLE [dbo].[Obresti_glava]
	CHECK CONSTRAINT [FK_Obresti_glava_Poslovni_partner]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Obresti_glava_Poslovni_partner1]
	FOREIGN KEY ([do_kupca_abc]) REFERENCES [dbo].[Poslovni_partner] ([sifra])
ALTER TABLE [dbo].[Obresti_glava]
	CHECK CONSTRAINT [FK_Obresti_glava_Poslovni_partner1]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
