/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Obresti_glava_obracun_devizni


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Obresti_glava_obracun_devizni]
Print 'Create Table [dbo].[Obresti_glava_obracun_devizni]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Obresti_glava_obracun_devizni] (
		[leto]                  [int] NOT NULL,
		[stevilka]              [int] NOT NULL,
		[stevilka_obracuna]     [int] NOT NULL,
		[naziv_partnerja]       [varchar](35) NOT NULL,
		[partner]               [varchar](7) NOT NULL,
		[valuta]                [varchar](6) NOT NULL,
		[obresti_znesek]        [money] NOT NULL,
		CONSTRAINT [PK_Obresti_glava_obracun_devizni]
		PRIMARY KEY
		CLUSTERED
		([leto], [stevilka], [partner], [valuta])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava_obracun_devizni] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava_obracun_devizni]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Obresti_glava_obracun_devizni_Obresti_glava]
	FOREIGN KEY ([leto], [stevilka]) REFERENCES [dbo].[Obresti_glava] ([leto], [stevilka])
ALTER TABLE [dbo].[Obresti_glava_obracun_devizni]
	CHECK CONSTRAINT [FK_Obresti_glava_obracun_devizni_Obresti_glava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_glava_obracun_devizni]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Obresti_glava_obracun_devizni_Poslovni_partner]
	FOREIGN KEY ([partner]) REFERENCES [dbo].[Poslovni_partner] ([sifra])
ALTER TABLE [dbo].[Obresti_glava_obracun_devizni]
	CHECK CONSTRAINT [FK_Obresti_glava_obracun_devizni_Poslovni_partner]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
