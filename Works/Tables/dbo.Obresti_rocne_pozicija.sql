/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Obresti_rocne_pozicija


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Obresti_rocne_pozicija]
Print 'Create Table [dbo].[Obresti_rocne_pozicija]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Obresti_rocne_pozicija] (
		[identity_roc_obrac]           [int] NOT NULL,
		[identity_roc_pozicija]        [int] IDENTITY(1, 1) NOT NULL,
		[stevika_dokumenta]            [varchar](12) NULL,
		[datum_valute]                 [smalldatetime] NULL,
		[datum_placila]                [smalldatetime] NULL,
		[stevilo_dni]                  [smallint] NULL,
		[znesek]                       [money] NULL,
		[sifra_valute]                 [varchar](6) NULL,
		[obresti_devizne]              [money] NULL,
		[upostevaj_prvi_dan]           [bit] NOT NULL,
		[obresti_navadne_tolarske]     [money] NULL,
		CONSTRAINT [PK_Obresti_rocne_pozicija]
		PRIMARY KEY
		NONCLUSTERED
		([identity_roc_obrac], [identity_roc_pozicija])
	WITH FILLFACTOR=90
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_rocne_pozicija]
	ADD
	CONSTRAINT [DF_Obresti_rocne_pozicija_upostevaj_prvi_dan]
	DEFAULT ((0)) FOR [upostevaj_prvi_dan]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_rocne_pozicija] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Obresti_rocne_pozicija]
	WITH NOCHECK
	ADD CONSTRAINT [FK_Obresti_rocne_pozicija_Obresti_rocne_glava]
	FOREIGN KEY ([identity_roc_obrac]) REFERENCES [dbo].[Obresti_rocne_glava] ([identity_roc_obrac])
ALTER TABLE [dbo].[Obresti_rocne_pozicija]
	CHECK CONSTRAINT [FK_Obresti_rocne_pozicija_Obresti_rocne_glava]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
