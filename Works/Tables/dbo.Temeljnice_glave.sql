/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  Temeljnice_glave


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Temeljnice_glave]
Print 'Create Table [dbo].[Temeljnice_glave]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Temeljnice_glave] (
		[leto]                    [smallint] NOT NULL,
		[vrsta_temeljnice_si]     [tinyint] NOT NULL,
		[stevilka_temeljnice]     [smallint] NOT NULL,
		[id]                      [int] IDENTITY(1, 1) NOT NULL,
		[mesec]                   [smallint] NOT NULL,
		[referent_si]             [tinyint] NOT NULL,
		[datum]                   [smalldatetime] NOT NULL,
		[zaprta]                  [bit] NOT NULL,
		[opis]                    [varchar](60) NULL,
		[int_status]              [tinyint] NULL,
		[dirty]                   [bit] NULL,
		[stevilo_knjizb]          [int] NULL,
		[stevilo_neknjizenih]     [int] NULL,
		[kumulativa_breme]        [money] NULL,
		[kumulativa_dobro]        [money] NULL,
		[kumulativa_saldo]        AS ([kumulativa_breme]-[kumulativa_dobro]),
		[stevilo_obdobij]         [tinyint] NULL,
		[zadnja_sprememba]        [datetime] NULL,
		[temeljnica_op]           AS ((((CONVERT([varchar](4),[leto])+'/')+right('0'+CONVERT([varchar](2),[vrsta_temeljnice_si]),(2)))+'/')+right('00'+CONVERT([varchar](3),[stevilka_temeljnice]),(3))),
		[casovna_znacka]          [datetime] NULL,
		[interni_komentar]        [varchar](60) NULL,
		CONSTRAINT [PK_Temeljnice_glave]
		PRIMARY KEY
		CLUSTERED
		([leto], [vrsta_temeljnice_si], [stevilka_temeljnice])
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Temeljnice_glave]
	ADD
	CONSTRAINT [DF_Temeljnice_glave_casovna_znacka]
	DEFAULT (getdate()) FOR [casovna_znacka]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Temeljnice_glave]
	ADD
	CONSTRAINT [DF_Temeljnice_glave_dirty]
	DEFAULT ((1)) FOR [dirty]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Temeljnice_glave]
	ADD
	CONSTRAINT [DF_Temeljnice_glave_vec_obdobij]
	DEFAULT ((1)) FOR [stevilo_obdobij]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Temeljnice_glave]
	ADD
	CONSTRAINT [DF_Temeljnice_glave_zaprta]
	DEFAULT ((0)) FOR [zaprta]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_bindefault @defname=N'[dbo].[fingerprint]', @objname=N'[dbo].[Temeljnice_glave].[interni_komentar]'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Temeljnice_glave] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
