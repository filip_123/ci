/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Tables:  sif_Identifikacijske_stevilke


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[sif_Identifikacijske_stevilke]
Print 'Create Table [dbo].[sif_Identifikacijske_stevilke]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[sif_Identifikacijske_stevilke] (
		[id]                               [int] IDENTITY(1, 1) NOT NULL,
		[identifikacijska_stevilka_id]     [int] NOT NULL,
		[identifikacijska_stevilka_si]     [varchar](14) NOT NULL,
		[drzava_id]                        [int] NULL,
		[drzava_si]                        [varchar](6) NOT NULL,
		[poslovni_partner_id]              [int] NULL,
		[poslovni_partner_si]              [varchar](7) NOT NULL,
		[interni_komentar]                 [varchar](60) NULL,
		[binary_hash]                      [int] NULL,
		CONSTRAINT [UK_sif_Ident_id]
		UNIQUE
		NONCLUSTERED
		([identifikacijska_stevilka_id])
		ON [PRIMARY],
		CONSTRAINT [PK_sif_Ident]
		PRIMARY KEY
		NONCLUSTERED
		([id])
	ON [PRIMARY]
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[sif_Identifikacijske_stevilke]
	ADD
	CONSTRAINT [DF_sif_Identifikacijske_stevilke_identifikacijska_stevilka_id]
	DEFAULT ((0)) FOR [identifikacijska_stevilka_id]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [ZX_sif_Identifikacijske_stevilke_si]
	ON [dbo].[sif_Identifikacijske_stevilke] ([identifikacijska_stevilka_si])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_bindefault @defname=N'[dbo].[fingerprint]', @objname=N'[dbo].[sif_Identifikacijske_stevilke].[interni_komentar]'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[sif_Identifikacijske_stevilke] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[sif_Identifikacijske_stevilke]
	WITH NOCHECK
	ADD CONSTRAINT [FK_sif_Ident_Drzava_si]
	FOREIGN KEY ([drzava_si]) REFERENCES [dbo].[Drzava] ([sifra])
ALTER TABLE [dbo].[sif_Identifikacijske_stevilke]
	CHECK CONSTRAINT [FK_sif_Ident_Drzava_si]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[sif_Identifikacijske_stevilke]
	WITH NOCHECK
	ADD CONSTRAINT [FK_sif_Ident_Poslovni_partner_si]
	FOREIGN KEY ([poslovni_partner_si]) REFERENCES [dbo].[Poslovni_partner] ([sifra])
ALTER TABLE [dbo].[sif_Identifikacijske_stevilke]
	CHECK CONSTRAINT [FK_sif_Ident_Poslovni_partner_si]

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
