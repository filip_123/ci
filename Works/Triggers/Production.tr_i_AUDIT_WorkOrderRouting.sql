/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.39
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_i_AUDIT_WorkOrderRouting


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [Production].[tr_i_AUDIT_WorkOrderRouting]
Print 'Create Trigger [Production].[tr_i_AUDIT_WorkOrderRouting]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [Production].[tr_i_AUDIT_WorkOrderRouting]
ON [Production].[WorkOrderRouting]
FOR INSERT


	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  INSERT TRIGGER for Table:  [Production].[WorkOrderRouting]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.14
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE				varchar(50),
		@AUDIT_LOG_TRANSACTION_ID		Int,
		@PRIM_KEY				nvarchar(4000),
		--@TABLE_NAME				nvarchar(4000),
		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[Production].[WorkOrderRouting]'
	Select @ROWS_COUNT=count(*) from inserted
	Set @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'WorkOrderRouting',
		'Production',
		2,	--	ACTION ID For INSERT
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'WorkOrderID',
		CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[WorkOrderID] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ProductID',
		CONVERT(nvarchar(4000), NEW.[ProductID], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ProductID] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'OperationSequence',
		CONVERT(nvarchar(4000), NEW.[OperationSequence], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[OperationSequence] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'LocationID',
		CONVERT(nvarchar(4000), NEW.[LocationID], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[LocationID] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ScheduledStartDate',
		CONVERT(nvarchar(4000), NEW.[ScheduledStartDate], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ScheduledStartDate] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ScheduledEndDate',
		CONVERT(nvarchar(4000), NEW.[ScheduledEndDate], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ScheduledEndDate] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ActualStartDate',
		CONVERT(nvarchar(4000), NEW.[ActualStartDate], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ActualStartDate] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ActualEndDate',
		CONVERT(nvarchar(4000), NEW.[ActualEndDate], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ActualEndDate] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ActualResourceHrs',
		CONVERT(nvarchar(4000), NEW.[ActualResourceHrs], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ActualResourceHrs] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'PlannedCost',
		CONVERT(nvarchar(4000), NEW.[PlannedCost], 2),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[PlannedCost] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ActualCost',
		CONVERT(nvarchar(4000), NEW.[ActualCost], 2),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ActualCost] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[WorkOrderID]='+CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0), '[WorkOrderID] Is Null') + ' AND ' + IsNull('[ProductID]='+CONVERT(nvarchar(4000), NEW.[ProductID], 0), '[ProductID] Is Null') + ' AND ' + IsNull('[OperationSequence]='+CONVERT(nvarchar(4000), NEW.[OperationSequence], 0), '[OperationSequence] Is Null')),
		'ModifiedDate',
		CONVERT(nvarchar(4000), NEW.[ModifiedDate], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[WorkOrderID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ProductID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[OperationSequence], 0))
	FROM inserted NEW
	WHERE NEW.[ModifiedDate] Is Not Null
    

	-- Lookup 
	

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  

End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[Production].[tr_i_AUDIT_WorkOrderRouting]', @order='Last', @stmttype='INSERT'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
