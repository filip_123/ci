/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.39
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_u_AUDIT_TransactionHistoryArchive


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [Production].[tr_u_AUDIT_TransactionHistoryArchive]
Print 'Create Trigger [Production].[tr_u_AUDIT_TransactionHistoryArchive]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [Production].[tr_u_AUDIT_TransactionHistoryArchive]
ON [Production].[TransactionHistoryArchive]
FOR UPDATE

	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  UPDATE TRIGGER for Table:  [Production].[TransactionHistoryArchive]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.13
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AUDIT_LOG_TRANSACTION_ID	Int,
		@PRIM_KEY			nvarchar(4000),
		@Inserted	    		bit,
		--@TABLE_NAME				nvarchar(4000),
 		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[Production].[TransactionHistoryArchive]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'TransactionHistoryArchive',
		'Production',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	SET @Inserted = 0
	
	If UPDATE([TransactionID])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'TransactionID',
			CONVERT(nvarchar(4000), OLD.[TransactionID], 0),
			CONVERT(nvarchar(4000), NEW.[TransactionID], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			WHERE (
			
			
				(
					NEW.[TransactionID] <>
					OLD.[TransactionID]
				) Or
			
				(
					NEW.[TransactionID] Is Null And
					OLD.[TransactionID] Is Not Null
				) Or
				(
					NEW.[TransactionID] Is Not Null And
					OLD.[TransactionID] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ProductID])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'ProductID',
			CONVERT(nvarchar(4000), OLD.[ProductID], 0),
			CONVERT(nvarchar(4000), NEW.[ProductID], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[ProductID] <>
					OLD.[ProductID]
				) Or
			
				(
					NEW.[ProductID] Is Null And
					OLD.[ProductID] Is Not Null
				) Or
				(
					NEW.[ProductID] Is Not Null And
					OLD.[ProductID] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ReferenceOrderID])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'ReferenceOrderID',
			CONVERT(nvarchar(4000), OLD.[ReferenceOrderID], 0),
			CONVERT(nvarchar(4000), NEW.[ReferenceOrderID], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[ReferenceOrderID] <>
					OLD.[ReferenceOrderID]
				) Or
			
				(
					NEW.[ReferenceOrderID] Is Null And
					OLD.[ReferenceOrderID] Is Not Null
				) Or
				(
					NEW.[ReferenceOrderID] Is Not Null And
					OLD.[ReferenceOrderID] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ReferenceOrderLineID])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'ReferenceOrderLineID',
			CONVERT(nvarchar(4000), OLD.[ReferenceOrderLineID], 0),
			CONVERT(nvarchar(4000), NEW.[ReferenceOrderLineID], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[ReferenceOrderLineID] <>
					OLD.[ReferenceOrderLineID]
				) Or
			
				(
					NEW.[ReferenceOrderLineID] Is Null And
					OLD.[ReferenceOrderLineID] Is Not Null
				) Or
				(
					NEW.[ReferenceOrderLineID] Is Not Null And
					OLD.[ReferenceOrderLineID] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TransactionDate])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'TransactionDate',
			CONVERT(nvarchar(4000), OLD.[TransactionDate], 121),
			CONVERT(nvarchar(4000), NEW.[TransactionDate], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[TransactionDate] <>
					OLD.[TransactionDate]
				) Or
			
				(
					NEW.[TransactionDate] Is Null And
					OLD.[TransactionDate] Is Not Null
				) Or
				(
					NEW.[TransactionDate] Is Not Null And
					OLD.[TransactionDate] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TransactionType])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'TransactionType',
			CONVERT(nvarchar(4000), OLD.[TransactionType], 0),
			CONVERT(nvarchar(4000), NEW.[TransactionType], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[TransactionType] <>
					OLD.[TransactionType]
				) Or
			
				(
					NEW.[TransactionType] Is Null And
					OLD.[TransactionType] Is Not Null
				) Or
				(
					NEW.[TransactionType] Is Not Null And
					OLD.[TransactionType] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Quantity])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'Quantity',
			CONVERT(nvarchar(4000), OLD.[Quantity], 0),
			CONVERT(nvarchar(4000), NEW.[Quantity], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[Quantity] <>
					OLD.[Quantity]
				) Or
			
				(
					NEW.[Quantity] Is Null And
					OLD.[Quantity] Is Not Null
				) Or
				(
					NEW.[Quantity] Is Not Null And
					OLD.[Quantity] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ActualCost])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'ActualCost',
			CONVERT(nvarchar(4000), OLD.[ActualCost], 2),
			CONVERT(nvarchar(4000), NEW.[ActualCost], 2),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[ActualCost] <>
					OLD.[ActualCost]
				) Or
			
				(
					NEW.[ActualCost] Is Null And
					OLD.[ActualCost] Is Not Null
				) Or
				(
					NEW.[ActualCost] Is Not Null And
					OLD.[ActualCost] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([ModifiedDate])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TransactionID]='+CONVERT(nvarchar(4000), IsNull(OLD.[TransactionID], NEW.[TransactionID]), 0), '[TransactionID] Is Null')),
		    'ModifiedDate',
			CONVERT(nvarchar(4000), OLD.[ModifiedDate], 121),
			CONVERT(nvarchar(4000), NEW.[ModifiedDate], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TransactionID], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TransactionID], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[TransactionID], 0)=CONVERT(nvarchar(4000), OLD.[TransactionID], 0) or (NEW.[TransactionID] Is Null and OLD.[TransactionID] Is Null))
			where (
			
			
				(
					NEW.[ModifiedDate] <>
					OLD.[ModifiedDate]
				) Or
			
				(
					NEW.[ModifiedDate] Is Null And
					OLD.[ModifiedDate] Is Not Null
				) Or
				(
					NEW.[ModifiedDate] Is Not Null And
					OLD.[ModifiedDate] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
	END

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  
End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[Production].[tr_u_AUDIT_TransactionHistoryArchive]', @order='Last', @stmttype='UPDATE'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
