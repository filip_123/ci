/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.40
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_i_AUDIT_Run_LastExecution


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [tSQLt].[tr_i_AUDIT_Run_LastExecution]
Print 'Create Trigger [tSQLt].[tr_i_AUDIT_Run_LastExecution]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [tSQLt].[tr_i_AUDIT_Run_LastExecution]
ON [tSQLt].[Run_LastExecution]
FOR INSERT


	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  INSERT TRIGGER for Table:  [tSQLt].[Run_LastExecution]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.15
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE				varchar(50),
		@AUDIT_LOG_TRANSACTION_ID		Int,
		@PRIM_KEY				nvarchar(4000),
		--@TABLE_NAME				nvarchar(4000),
		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[tSQLt].[Run_LastExecution]'
	Select @ROWS_COUNT=count(*) from inserted
	Set @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'Run_LastExecution',
		'tSQLt',
		2,	--	ACTION ID For INSERT
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[TestName]=N'''+replace(CONVERT(nvarchar(4000), NEW.[TestName], 0), '''', '''''')+'''', '[TestName] Is Null') + ' AND ' + IsNull('[SessionId]='+CONVERT(nvarchar(4000), NEW.[SessionId], 0), '[SessionId] Is Null') + ' AND ' + IsNull('[LoginTime]=N'''+replace(CONVERT(nvarchar(4000), NEW.[LoginTime], 121), '''', '''''')+'''', '[LoginTime] Is Null')),
		'TestName',
		CONVERT(nvarchar(4000), NEW.[TestName], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TestName], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[SessionId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoginTime], 121))
	FROM inserted NEW
	WHERE NEW.[TestName] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[TestName]=N'''+replace(CONVERT(nvarchar(4000), NEW.[TestName], 0), '''', '''''')+'''', '[TestName] Is Null') + ' AND ' + IsNull('[SessionId]='+CONVERT(nvarchar(4000), NEW.[SessionId], 0), '[SessionId] Is Null') + ' AND ' + IsNull('[LoginTime]=N'''+replace(CONVERT(nvarchar(4000), NEW.[LoginTime], 121), '''', '''''')+'''', '[LoginTime] Is Null')),
		'SessionId',
		CONVERT(nvarchar(4000), NEW.[SessionId], 0),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TestName], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[SessionId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoginTime], 121))
	FROM inserted NEW
	WHERE NEW.[SessionId] Is Not Null
    
	INSERT INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
	(
		AUDIT_LOG_TRANSACTION_ID,
		PRIMARY_KEY_DATA,
		COL_NAME,
		NEW_VALUE_LONG,
		DATA_TYPE
		, KEY1, KEY2, KEY3
	)
	SELECT
		@AUDIT_LOG_TRANSACTION_ID,
		convert(nvarchar(1500), IsNull('[TestName]=N'''+replace(CONVERT(nvarchar(4000), NEW.[TestName], 0), '''', '''''')+'''', '[TestName] Is Null') + ' AND ' + IsNull('[SessionId]='+CONVERT(nvarchar(4000), NEW.[SessionId], 0), '[SessionId] Is Null') + ' AND ' + IsNull('[LoginTime]=N'''+replace(CONVERT(nvarchar(4000), NEW.[LoginTime], 121), '''', '''''')+'''', '[LoginTime] Is Null')),
		'LoginTime',
		CONVERT(nvarchar(4000), NEW.[LoginTime], 121),
		'A'
		, CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TestName], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[SessionId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoginTime], 121))
	FROM inserted NEW
	WHERE NEW.[LoginTime] Is Not Null
    

	-- Lookup 
	

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  

End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[tSQLt].[tr_i_AUDIT_Run_LastExecution]', @order='Last', @stmttype='INSERT'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
