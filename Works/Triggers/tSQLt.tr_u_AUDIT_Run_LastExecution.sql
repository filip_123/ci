/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.39
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_u_AUDIT_Run_LastExecution


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [tSQLt].[tr_u_AUDIT_Run_LastExecution]
Print 'Create Trigger [tSQLt].[tr_u_AUDIT_Run_LastExecution]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [tSQLt].[tr_u_AUDIT_Run_LastExecution]
ON [tSQLt].[Run_LastExecution]
FOR UPDATE

	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  UPDATE TRIGGER for Table:  [tSQLt].[Run_LastExecution]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.15
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AUDIT_LOG_TRANSACTION_ID	Int,
		@PRIM_KEY			nvarchar(4000),
		@Inserted	    		bit,
		--@TABLE_NAME				nvarchar(4000),
 		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[tSQLt].[Run_LastExecution]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'Run_LastExecution',
		'tSQLt',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	SET @Inserted = 0
	
	If UPDATE([TestName])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TestName]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[TestName], NEW.[TestName]), 0),'''' ,'''''')+'''', '[TestName] Is Null')+' AND '+IsNull('[SessionId]='+CONVERT(nvarchar(4000), IsNull(OLD.[SessionId], NEW.[SessionId]), 0), '[SessionId] Is Null')+' AND '+IsNull('[LoginTime]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[LoginTime], NEW.[LoginTime]), 121),'''' ,'''''')+'''', '[LoginTime] Is Null')),
		    'TestName',
			CONVERT(nvarchar(4000), OLD.[TestName], 0),
			CONVERT(nvarchar(4000), NEW.[TestName], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TestName], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TestName], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[SessionId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[SessionId], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoginTime], 121)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoginTime], 121)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[TestName], 0)=CONVERT(nvarchar(4000), OLD.[TestName], 0) or (NEW.[TestName] Is Null and OLD.[TestName] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[SessionId], 0)=CONVERT(nvarchar(4000), OLD.[SessionId], 0) or (NEW.[SessionId] Is Null and OLD.[SessionId] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[LoginTime], 121)=CONVERT(nvarchar(4000), OLD.[LoginTime], 121) or (NEW.[LoginTime] Is Null and OLD.[LoginTime] Is Null))
			WHERE (
			
			
				(
					NEW.[TestName] <>
					OLD.[TestName]
				) Or
			
				(
					NEW.[TestName] Is Null And
					OLD.[TestName] Is Not Null
				) Or
				(
					NEW.[TestName] Is Not Null And
					OLD.[TestName] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([SessionId])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TestName]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[TestName], NEW.[TestName]), 0),'''' ,'''''')+'''', '[TestName] Is Null')+' AND '+IsNull('[SessionId]='+CONVERT(nvarchar(4000), IsNull(OLD.[SessionId], NEW.[SessionId]), 0), '[SessionId] Is Null')+' AND '+IsNull('[LoginTime]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[LoginTime], NEW.[LoginTime]), 121),'''' ,'''''')+'''', '[LoginTime] Is Null')),
		    'SessionId',
			CONVERT(nvarchar(4000), OLD.[SessionId], 0),
			CONVERT(nvarchar(4000), NEW.[SessionId], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TestName], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TestName], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[SessionId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[SessionId], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoginTime], 121)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoginTime], 121)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[TestName], 0)=CONVERT(nvarchar(4000), OLD.[TestName], 0) or (NEW.[TestName] Is Null and OLD.[TestName] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[SessionId], 0)=CONVERT(nvarchar(4000), OLD.[SessionId], 0) or (NEW.[SessionId] Is Null and OLD.[SessionId] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[LoginTime], 121)=CONVERT(nvarchar(4000), OLD.[LoginTime], 121) or (NEW.[LoginTime] Is Null and OLD.[LoginTime] Is Null))
			WHERE (
			
			
				(
					NEW.[SessionId] <>
					OLD.[SessionId]
				) Or
			
				(
					NEW.[SessionId] Is Null And
					OLD.[SessionId] Is Not Null
				) Or
				(
					NEW.[SessionId] Is Not Null And
					OLD.[SessionId] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([LoginTime])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1, KEY2, KEY3
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[TestName]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[TestName], NEW.[TestName]), 0),'''' ,'''''')+'''', '[TestName] Is Null')+' AND '+IsNull('[SessionId]='+CONVERT(nvarchar(4000), IsNull(OLD.[SessionId], NEW.[SessionId]), 0), '[SessionId] Is Null')+' AND '+IsNull('[LoginTime]=N'''+replace(CONVERT(nvarchar(4000), IsNull(OLD.[LoginTime], NEW.[LoginTime]), 121),'''' ,'''''')+'''', '[LoginTime] Is Null')),
		    'LoginTime',
			CONVERT(nvarchar(4000), OLD.[LoginTime], 121),
			CONVERT(nvarchar(4000), NEW.[LoginTime], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[TestName], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[TestName], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[SessionId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[SessionId], 0))), IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoginTime], 121)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoginTime], 121)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[TestName], 0)=CONVERT(nvarchar(4000), OLD.[TestName], 0) or (NEW.[TestName] Is Null and OLD.[TestName] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[SessionId], 0)=CONVERT(nvarchar(4000), OLD.[SessionId], 0) or (NEW.[SessionId] Is Null and OLD.[SessionId] Is Null)) AND (CONVERT(nvarchar(4000), NEW.[LoginTime], 121)=CONVERT(nvarchar(4000), OLD.[LoginTime], 121) or (NEW.[LoginTime] Is Null and OLD.[LoginTime] Is Null))
			WHERE (
			
			
				(
					NEW.[LoginTime] <>
					OLD.[LoginTime]
				) Or
			
				(
					NEW.[LoginTime] Is Null And
					OLD.[LoginTime] Is Not Null
				) Or
				(
					NEW.[LoginTime] Is Not Null And
					OLD.[LoginTime] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
	END

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  
End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[tSQLt].[tr_u_AUDIT_Run_LastExecution]', @order='Last', @stmttype='UPDATE'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
