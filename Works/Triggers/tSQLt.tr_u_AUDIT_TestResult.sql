/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.39
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Triggers:  tr_u_AUDIT_TestResult


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Trigger [tSQLt].[tr_u_AUDIT_TestResult]
Print 'Create Trigger [tSQLt].[tr_u_AUDIT_TestResult]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TRIGGER [tSQLt].[tr_u_AUDIT_TestResult]
ON [tSQLt].[TestResult]
FOR UPDATE

	NOT FOR REPLICATION

As
-- "<TAG>SQLAUDIT GENERATED - DO NOT REMOVE</TAG>"
-- --------------------------------------------------------------------------------------------------------------
-- Legal:        You may freely edit and modify this template and make copies of it.
-- Description:  UPDATE TRIGGER for Table:  [tSQLt].[TestResult]
-- Author:       ApexSQL Software
-- Date:		 22.12.2016. 17.31.16
-- --------------------------------------------------------------------------------------------------------------
BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AUDIT_LOG_TRANSACTION_ID	Int,
		@PRIM_KEY			nvarchar(4000),
		@Inserted	    		bit,
		--@TABLE_NAME				nvarchar(4000),
 		@ROWS_COUNT				int

	SET NOCOUNT ON

	--Set @TABLE_NAME = '[tSQLt].[TestResult]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

	INSERT
	INTO [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS
	(
		TABLE_NAME,
		TABLE_SCHEMA,
		AUDIT_ACTION_ID,
		HOST_NAME,
		APP_NAME,
		MODIFIED_BY,
		MODIFIED_DATE,
		AFFECTED_ROWS,
		[DATABASE]
	)
	values(
		'TestResult',
		'tSQLt',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		'AdventureWorks2014'
	)

	
	Set @AUDIT_LOG_TRANSACTION_ID = SCOPE_IDENTITY()
	

	SET @Inserted = 0
	
	If UPDATE([Id])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'Id',
			CONVERT(nvarchar(4000), OLD.[Id], 0),
			CONVERT(nvarchar(4000), NEW.[Id], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Full Outer Join inserted NEW On
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			WHERE (
			
			
				(
					NEW.[Id] <>
					OLD.[Id]
				) Or
			
				(
					NEW.[Id] Is Null And
					OLD.[Id] Is Not Null
				) Or
				(
					NEW.[Id] Is Not Null And
					OLD.[Id] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Class])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'Class',
			CONVERT(nvarchar(4000), OLD.[Class], 0),
			CONVERT(nvarchar(4000), NEW.[Class], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[Class] <>
					OLD.[Class]
				) Or
			
				(
					NEW.[Class] Is Null And
					OLD.[Class] Is Not Null
				) Or
				(
					NEW.[Class] Is Not Null And
					OLD.[Class] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TestCase])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'TestCase',
			CONVERT(nvarchar(4000), OLD.[TestCase], 0),
			CONVERT(nvarchar(4000), NEW.[TestCase], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[TestCase] <>
					OLD.[TestCase]
				) Or
			
				(
					NEW.[TestCase] Is Null And
					OLD.[TestCase] Is Not Null
				) Or
				(
					NEW.[TestCase] Is Not Null And
					OLD.[TestCase] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TranName])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'TranName',
			CONVERT(nvarchar(4000), OLD.[TranName], 0),
			CONVERT(nvarchar(4000), NEW.[TranName], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[TranName] <>
					OLD.[TranName]
				) Or
			
				(
					NEW.[TranName] Is Null And
					OLD.[TranName] Is Not Null
				) Or
				(
					NEW.[TranName] Is Not Null And
					OLD.[TranName] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Result])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'Result',
			CONVERT(nvarchar(4000), OLD.[Result], 0),
			CONVERT(nvarchar(4000), NEW.[Result], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[Result] <>
					OLD.[Result]
				) Or
			
				(
					NEW.[Result] Is Null And
					OLD.[Result] Is Not Null
				) Or
				(
					NEW.[Result] Is Not Null And
					OLD.[Result] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([Msg])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'Msg',
			CONVERT(nvarchar(4000), OLD.[Msg], 0),
			CONVERT(nvarchar(4000), NEW.[Msg], 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[Msg] <>
					OLD.[Msg]
				) Or
			
				(
					NEW.[Msg] Is Null And
					OLD.[Msg] Is Not Null
				) Or
				(
					NEW.[Msg] Is Not Null And
					OLD.[Msg] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TestStartTime])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'TestStartTime',
			CONVERT(nvarchar(4000), OLD.[TestStartTime], 121),
			CONVERT(nvarchar(4000), NEW.[TestStartTime], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[TestStartTime] <>
					OLD.[TestStartTime]
				) Or
			
				(
					NEW.[TestStartTime] Is Null And
					OLD.[TestStartTime] Is Not Null
				) Or
				(
					NEW.[TestStartTime] Is Not Null And
					OLD.[TestStartTime] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE([TestEndTime])
	BEGIN
    
		INSERT
		INTO [AdventureWorks2014].dbo.AUDIT_LOG_DATA
		(
			AUDIT_LOG_TRANSACTION_ID,
			PRIMARY_KEY_DATA,
			COL_NAME,
			OLD_VALUE_LONG,
			NEW_VALUE_LONG,
			DATA_TYPE
			, KEY1
		)
		SELECT
			@AUDIT_LOG_TRANSACTION_ID,
		    convert(nvarchar(1500), IsNull('[Id]='+CONVERT(nvarchar(4000), IsNull(OLD.[Id], NEW.[Id]), 0), '[Id] Is Null')),
		    'TestEndTime',
			CONVERT(nvarchar(4000), OLD.[TestEndTime], 121),
			CONVERT(nvarchar(4000), NEW.[TestEndTime], 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[Id], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[Id], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[Id], 0)=CONVERT(nvarchar(4000), OLD.[Id], 0) or (NEW.[Id] Is Null and OLD.[Id] Is Null))
			where (
			
			
				(
					NEW.[TestEndTime] <>
					OLD.[TestEndTime]
				) Or
			
				(
					NEW.[TestEndTime] Is Null And
					OLD.[TestEndTime] Is Not Null
				) Or
				(
					NEW.[TestEndTime] Is Not Null And
					OLD.[TestEndTime] Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM [AdventureWorks2014].dbo.AUDIT_LOG_TRANSACTIONS WHERE AUDIT_LOG_TRANSACTION_ID = @AUDIT_LOG_TRANSACTION_ID
	END

  -- Restore @@IDENTITY Value
  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
  
End
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
EXEC sp_settriggerorder @triggername= '[tSQLt].[tr_u_AUDIT_TestResult]', @order='Last', @stmttype='UPDATE'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
