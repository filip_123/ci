/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0003
DATE:      01-06-2017 15.30.38
SERVER:    LAPTOP-0V4NFPAI\SQLEXPRESS2014

DATABASE:	AdventureWorks2014
  Views:  AUDIT_VIEW


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create View [dbo].[AUDIT_VIEW]
Print 'Create View [dbo].[AUDIT_VIEW]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE VIEW dbo.AUDIT_VIEW AS
/* ------------------------------------------------------------
VIEW:          AUDIT_VIEW
DESCRIPTION:   Selects Audit Log records and groups by MODIFIED_DATE and PK
               effectively grouping audit data by Audit transaction
   ------------------------------------------------------------ */
SELECT MAX(t.TABLE_NAME) AS TABLE_NAME,
    CASE MAX(t.AUDIT_ACTION_ID) 
    WHEN 1 THEN 'UPDATE' 
    WHEN 2 THEN 'INSERT' 
    WHEN 3 THEN 'DELETE' 
    END AS ACTION, 
    MAX(t.MODIFIED_BY) AS MODIFIED_BY, 
    MAX(PRIMARY_KEY_DATA) AS PRIMARY_KEY,
    COUNT(DISTINCT PRIMARY_KEY_DATA) AS REC_COUNT,
    CONVERT(varchar(20), MODIFIED_DATE, 113) AS MODIFIED_DATE,
    Max(HOST_NAME) AS COMPUTER,
    Max(APP_NAME) as APPLICATION
FROM dbo.AUDIT_LOG_TRANSACTIONS t
INNER JOIN dbo.AUDIT_LOG_DATA r ON r.AUDIT_LOG_TRANSACTION_ID = t.AUDIT_LOG_TRANSACTION_ID
GROUP BY MODIFIED_DATE, PRIMARY_KEY_DATA
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
